# vim:set fileencoding=utf-8
"""

    Marginal costs extraction from MarketOutput files
    =================================================

    Introduction
    ------------

    For Losses Monetization and |CO2| monetization modules, **Marginal Costs**
    need be extracted from MarketOutput files.

    **Marginal Costs** are needed to monetize losses and to find the marginal generating unit for |CO2|.

    Input File
    ----------

    Only MarketOutput files (usually named ``MMStandardOutputFile``, with
    ``.xlsx`` or ``.xlsb`` extension) following the common Template will be
    handled properly.


    .. note::

       The ``MMStandardOutputFile`` has to contain a worksheet named ``Hourly Market Data``.

    Output File
    -----------

    An output file will be created:

    - its name will be the name of the **MMStandardOutputFile**, prefixed by ``mglcost_`` and with a ``.csv`` extension,
    - it will contain only **Mgl Cost** data, for every market node and every Hour of the simulation


    .. |CO2| replace:: CO\ :sub:`2`
"""
import re
from pathlib import Path
import tempfile
import traceback

import pandas as pd

try:
    import win32com.client as win32
    from win32com.shell import shell, shellcon
    import pythoncom
except ImportError:
    # Pour tests Linux
    pass
try:
    import wx
except ImportError:
    # Pour tests Linux
    pass

try:
    import createDialogBoxes_wx
except ImportError:
    pass

import utils
import batch_wx
from ExceptionPTDF import ExceptionPTDF

PARAMETRES_DIALOG_BOXES = [
    {
        "text": "Market output simulation file (.xlsb, .xlsx) :",
        "action": ["open", {"filetypes": [("xlsb/xlsx files", "*.xlsb;*.xlsx")]}],
        "tooltip": "Name of Template file containing Market Simulations (MMStandardOutputFile, .xlsx or .xlsb file)",
        "varConfig": "template_filename",
        "help": r"help/html/extract_market_output.html#output-file",
    }
]


def start_batch():
    """TODO: Docstring for start_batch.
    """
    param_batch_dialogbox = [
        {
            "text": "Fichier batch",
            "action": ["open", {"filetypes": [("csv files", "*.csv")]}],
            "varConfig": "batch_filename",
            "help": "get_parameters",
        }
    ]

    batch_wx.batch(
        title="Extract csv from market template files",
        parameters=PARAMETRES_DIALOG_BOXES,
        module_name=Path(__file__).name,
        parametres_batch=param_batch_dialogbox,
        start_function=extract_mglcosts,
    )


def start_ihm():
    frame = wx.Frame(None, -1)
    createDialogBoxes_wx.make_dialog(
        wx.Panel,
        parent=frame,
        title="Extraction des données marché",
        module_name=Path(__file__).name,
        parameters=PARAMETRES_DIALOG_BOXES,
        start_function=extract_mglcosts,
        with_status_bar=True,
    )
    frame.Fit()
    frame.SetMinSize(frame.GetSize())
    frame.Centre()
    frame.Show()


def get_starting_row(filename, pattern):
    """Recherche la ligne d'entete d'un fichier à partir d'un pattern.

    Args:
        filename (str): nom du fichier
        pattern (list): liste de pattern à rechercher dans chaque ligne du fichier

    Returns:
        - le numéro de ligne de l'entete
        - l'entete


    """
    with utils.FileOrZipfile(filename, "r") as file_id:
        lines = pd.read_csv(
            file_id,
            dtype=str,
            encoding=utils.get_pd_encoding(file_id),
            sep=",",
            nrows=30,
            squeeze=True,
            header=None,
        )
    for idx, line in lines.iterrows():
        if any(pat in line.values for pat in pattern):
            return idx, list(line)
    return -1, None


def add_year_hour_date(dataframe):
    """Ajout des champs Hour, Year, Date au dataframe.

    Args:
        dataframe (pd.DataFrame): le dataframe auquel ajouter les colonnes Year, Hour, Date

    Returns: le dataframe modifié

    """
    hours = dataframe["Hour"].astype(int)
    date_month_hhmm = (
        pd.to_datetime(hours - 1, unit="h").dt.strftime("%d%b%H:%M").str.upper()
    )
    dataframe.insert(0, "Date", date_month_hhmm)
    dataframe.insert(0, "Year", 1)

    return dataframe


def extract_areas(filename):
    """Extraction des données marché par pays à partir du template.

    Args:
        filename (str): nom du fichier csv à traiter

    Returns:
        Un dataframe contenant toutes les données marché par pays, mises
        en forme pour le datafilter.

    """
    header_line, cols = get_starting_row(filename=filename, pattern=["FR_1", "FR00_1"])
    # Read the file
    with utils.FileOrZipfile(filename, "r") as file_id:
        areas_data = (
            pd.read_csv(
                file_id,
                dtype=str,
                sep=",",
                encoding=utils.get_pd_encoding(file_id),
                header=header_line,
                usecols=lambda x: x.endswith(("Hour", "Mgl Cost")),
            )
            # Suppression lignes dont la 1ere colonne (Hour) ne
            # contient aucune donnée
            .dropna(subset=[cols[0]])
            # Suppression des colonnes vides (entete Unnamed)
            .pipe(utils.drop_unnamed_cols)
            # On remplit les cellules vides par des 0
            .fillna("0")
            # Suppression des colonnes vides (colonnes textuelles)
            .dropna(axis="columns")
            # On trie les colonnes par ordre alphabétique
            .sort_index(axis=1)
            # Et on ajoute les colonnes Year, Hour, Date au début
            .pipe(add_year_hour_date)
        )
    return areas_data


def extract_mglcosts(
    template_filename, status_bar=None, log_std=None, log_err=None, output_only=False
):
    """TODO: Docstring for extract_market_data_from_template.

    Args:
        template_filename (TODO): TODO
        market_simulator (TODO): TODO
        status_bar (TODO): TODO
        log_std (TODO): TODO
        log_err (TODO): TODO

    Keyword Args:
        output_only (TODO): TODO

    Returns: TODO

    """
    try:
        status_bar.SetStatusText("")
        parent = status_bar.GetParent()
    except AttributeError:
        parent = None

    template_name = Path(template_filename).name
    msg = ("Info", "", "Reading Excel file " + template_name)
    utils.log_or_display_msg(
        msg, log_std, log_err, parent=parent, output_only=True, to_status_bar=True
    )

    # Open the xlsb/xlsx file using Excel
    pythoncom.CoInitialize()
    try:
        excel = win32.gencache.EnsureDispatch("Excel.Application")
    except AttributeError:
        print(traceback.format_exc())
        from shutil import rmtree
        import sys

        # Remove cache and try again.
        # delete modules refering to gen_py 
        module_list = [m.__name__ for m in sys.modules.values()]
        for module in module_list:
            if re.match(r'win32com\.gen_py\..+', module):
                print(module)
                del sys.modules[module]

        # Remove cache and try again.
        local_appdata_path = shell.SHGetFolderPath(
            0, shellcon.CSIDL_LOCAL_APPDATA, 0, 0
        )
        cache_dir = Path(local_appdata_path) / "Temp" / "gen_py"
        print(cache_dir)
        rmtree(cache_dir, ignore_errors=True)

        import importlib
        importlib.reload(win32)
        excel = win32.gencache.EnsureDispatch("Excel.Application")
        print(excel)

    excel.DisplayAlerts = False
    wbs = excel.Workbooks
    workbook = wbs.Open(template_filename)
    with tempfile.TemporaryDirectory(prefix="market_file_") as tmp_dir:
        areas_file = Path(tmp_dir) / (
            "mglcost_" + Path(template_filename).stem + ".csv"
        )

        for i in range(1, workbook.Sheets.Count + 1):
            # Save areas file in csv (with , as separator)
            if "Market" in workbook.Sheets[i].Name:
                msg = ("Info", "", "Saving file " + Path(areas_file).name)
                utils.log_or_display_msg(
                    msg,
                    log_std,
                    log_err,
                    parent=parent,
                    output_only=True,
                    to_status_bar=True,
                )
                workbook.Sheets[i].SaveAs(Filename=areas_file, FileFormat=6)
        excel.ActiveWorkbook.Close(False)
        del workbook
        del wbs

        msg = (
            "Info",
            "",
            "Start extracting Mgl Cost from " + Path(areas_file).name,
        )
        utils.log_or_display_msg(
            msg, log_std, log_err, parent=parent, output_only=True, to_status_bar=True
        )
        # Extract areas file
        areas_data = extract_areas(filename=areas_file)

        # Création des fichiers de sortie csv areas
        for data_file, dataframe in zip([areas_file], [areas_data]):
            outputfile = Path(data_file).stem + ".csv"
            msg = ("Info", "", "File " + outputfile + " created.")
            utils.log_or_display_msg(
                msg, log_std, log_err, parent=parent, output_only=True
            )
            dataframe.to_csv(
                Path(template_filename).parent / outputfile, sep=";", index=False
            )

        msg = ("Info", "", "End of extraction")
        utils.log_or_display_msg(
            msg,
            log_std,
            log_err,
            output_only=output_only,
            parent=parent,
        )


if __name__ == "__main__":
    root = wx.App()
    start_ihm()
    root.MainLoop()
