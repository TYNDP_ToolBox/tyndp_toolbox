﻿# vim: set fileencoding=utf-8
import gettext
import sys
from pathlib import Path
try:
    if hasattr(sys, "frozen"):
        current_dir = Path(sys.executable).parent
    else:
        current_dir = Path(__file__).resolve().parent
    gettext.find("translation")
    traduction = gettext.translation(
        "translation", localedir=current_dir / "locale", languages=["fr"]
    )
    traduction.install()
except:
    gettext.install("translations")
from json import load
import multiprocessing
import time
import shutil
from subprocess import Popen
import traceback
from zipfile import ZipFile

import tempfile
import matplotlib
import pythoncom
from win32com.shell import shell, shellcon

matplotlib.use("wxAgg")
import wx
import wx.html2

import losses_monetization_batch
import losses_monetization_gui
import _version


__version__ = _version.__version__

if __name__ == "__main__":
    # La ligne suivante est obligatoire dans le cas où un exécutable utilisant
    # le module multiprocessing doit être généré
    multiprocessing.freeze_support()

    # Par défaut : on lance le mode normal de l'IHM PTDF
    mode_batch = False
    # On récupère l'option de la ligne de commande
    if len(sys.argv) > 1:
        # Si argument batch : lancement de l'IHM en mode batch
        mode_batch = sys.argv[1] == "batch"

    app = wx.App()
    if mode_batch:
        frame = losses_monetization_batch.IhmPTDFBatch(None)
    else:
        frame = losses_monetization_gui.IhmPTDFNormal(None)
    frame.Show()
    app.MainLoop()
