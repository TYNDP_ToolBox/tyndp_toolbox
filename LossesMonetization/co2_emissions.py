# -*- coding: utf-8 -*-
"""

    |CO2| monetization
    ==================

    Introduction
    ------------

    Currently |CO2| is implicitly taken into account via the European Emissions
    Trading Scheme (ETS) within SEW and losses.

    The marginal price used for market simulations is based on the EU ETS |CO2|
    price.

    For example during TYNDP2016 and TYNDP18, |CO2| price does not have the
    same value for each scenario:

    ==============================  ==  ==  ==  ==  ======  ======  ======  ========
    TYNDP2016                                       TYNDP2018
    ----------------------------------------------  --------------------------------
    Scenario                        V1  V2  V3  V4  BE2025  ST2030  DG2030  EUCO2030
    ==============================  ==  ==  ==  ==  ======  ======  ======  ========
    **EU ETS CO2 price (€ / ton)**  17  17  71  76      26      84      50        27
    ==============================  ==  ==  ==  ==  ======  ======  ======  ========

    The marginal costs are computed like this: fuel cost + EU ETS |CO2| price.
    For example in ST 2030 a CCGT which emits 0,3538 ton/MWh at 84€/ton and a
    fuel cost at 56,3 €/MWh will have as marginal cost: 56,3+0,3538x84 = 86€/MWh

    This EU ETC price captures an impact of |CO2| but not the whole impact. The
    societal cost is the cost pathway to use in order to reach global climate
    goals.
    Our methodology is based on a post processing and not to change the value
    of |CO2| in our simulations. Because integrating the societal cost of |CO2| and
    changing the merit order with a highest marginal cost will provide wrong
    results:

    -  In real life, the producers fix their marginal cost using the EU ETS
       price and not the societal cost
    -  Consequently, the merit order depends on the EU ETS price and not the
       complete |CO2| societal cost.
    -  The market simulations aim at modelling the exchange and generation plan
       that would occur in the future under certain hypotheses
    -  Using a societal cost in these simulations would not reflect what would
       occur and **could lead to wrong investment!**
    -  However, such simulations could give some good idea of the impact of EU
       ETS price on the |CO2| emission of the European electric system, especially
       to enlighten politicians and citizens.

    So we would like to use 2 additional ext post consideration through a |CO2| societal cost:

    -  SEW
       Value |CO2| emission variation resulting from **change of generation plans**
       to the difference between societal cost and EU ETS price
    -  Losses
       Value |CO2| emission variation resulting from **change of losses volumes** to
       the difference between societal cost and EU ETS price

    **Within this methodology no other simulations needed.
    DT CBA will provide the values of Social cost in the CBA3.0.**



    Example of |CO2| monetization for SEW and Losses
    ------------------------------------------------

    Here is an example of |CO2| monetization for SEW and for losses:

    .. image:: images/co2_emissions_sew_example.png

    Project sheets of each project provide us with the |CO2| emission, this
    emission multiplied by the difference of the societal cost and ETS price.
    This computation has to be done for each scenario and for each project.

    .. image:: images/co2_emissions_losses_example.png

    Monetization of |CO2| due to losses
    -----------------------------------

    In order to monetize |CO2| emissions, we will need, for every hour of market simulations, to:

    - determine the marginal fuel type generations for every market nodes,
    - calculate the |CO2| emissions (in ton / MW) for every market nodes,
    - based on losses calculations per market node, calculate |CO2| emissions (in tons)
    - calculate monetization of |CO2| due to losses.

    Finding marginal fuel type generation
    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    First approach: marginal generation is the closest to marginal prices
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Market simulations outputs contain marginal prices for every market nodes
    of the simulation.

    As a first approach, we could have considered that the fuel type whose generation
    cost is the closest to these marginal prices would be the marginal fuel
    type generation. However, we have to take into account:

    - the variability of marginal prices (due to integration of hurdle costs into marginal prices for example),
    - in some scenario some fuel type have very close fuel price: in some
      cases, it will be impossible to decide what is the marginal generation
      type,

      .. image:: images/co2_emissions_close_fuel_type.png

    - in some cases, it might occur that the selected marginal generation is
      a fuel type generation with very low installed capacities: it would be
      very unlikely that this kind of generation will be the marginal
      generation.

      For example, in the screenshot above, the closest generation type for
      most of the hours would be `Oil shale old` (installed capacity in EUCO
      scenario: 756 MW, where the other closest generation types have higher
      installed capacities (Hard coal old 2: 26 GW and Gas CCGT old 2: 137 GW)).

      If these fuel types generations have had close values for |CO2|
      emissions, it wouldn't have been a big issue. But *Oil shale - old* has
      around 1.2 ton/MWh as |CO2| emissions factor, whereas for *Gas - CCGT old 2*
      it is around 0.5 ton/MWh: a mistake on the marginal generation fuel
      type can then lead to over (or under)-estimate |CO2| emissions.

    **For all these reasons, this first approach hasn't been chosen.**

    Prefered approach: Clusterization of generation types with close fuel price
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    In order to avoid the drawbacks exposed above, an approach based on
    clusterization has been chosen:

    - marginal generation types with close fuel prices will be clustered:
      the tests have concluded that for the 2030 scenarios, a
      **good thresold is 2 € / MWh.**
    - the cluster will have fuel price and |CO2| emissions values based on the
      average of every clustered fuel generation wheighted by their installed
      capacity.

      .. image:: images/co2_emissions_clustered_fuel_type.png

    For example, the green cluster on the screenshot above is calculated based on the following values:

      .. image:: images/co2_emissions_example_clustered_fuel_type.png

    The marginal price of the cluster replacing the 3 fuel types is calculated with:

    .. math::

      \mathit{Marginal \ Price(cluster)} = \\frac{64.84 \cdot 25897 + 64.89 \cdot 137460 + 65.37 \cdot 756}{25897 + 137460 + 756}

    And the same principle will apply to calculate |CO2| emissions.


    Calculating |CO2| emissions (in tons / MWh)
    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    For every fuel type generation (or *clustered* generation), |CO2| emissions
    in tons / MWh are calculated as follows:

    .. math::

       \mathit{CO_2 emissions (tons / MWh)} = \\frac{3.6}{1000} \cdot \\frac{\mathit{CO_2 \ emission \ factor (kg / NetGJ)}}{\mathit{Standard \ efficiency \ in \ NCV \ terms (\%)}}

    where:

    - :math:`\mathit{CO_2 \ emissions}` are in tons / MWh
    - :math:`\mathit{CO_2 \ emission \ factor}` is in kg / Net GJ

    |CO2| monetization
    ^^^^^^^^^^^^^^^^^^

    .. math::

        \mathit{CO_2 \ monetization} = (\mathit{Societal \ Price} - \mathit{ETS \ price}) \cdot \sum_{h=1}^{8736}\sum_{i=1}^{n} losses(h, i) \cdot \mathit{CO_2 \ emissions(h, i)}

    where:

    - :math:`{CO_2 \ monetization}` is in €,
    - *losses(h, i)* are the losses (in MW) for hour *h* and market zone *i*,
    - :math:`{CO_2 \ emissions(h, i)}` are the |CO2| emissions (in tons / MWh) for the
      marginal fuel type of the hour *h* and market zone *i*,
    - *Societal Price* and *ETS Price* are in € / ton.

    In order to assess the impact of |CO2| due to losses for a project,
    :math:`{CO_2 \ monetization}` has to be computed **with the project** and
    **without the project**.

    To avoid double counting of losses, we have to take out the losses already
    included in load when we compute the delta, hour per hour and per market
    zone:

    .. math::

        \mathit{Correction \ (Euro)} = (\mathit{Societal \ Price} - \mathit{ETS \ Price}) \cdot \sum_{h=1}^{8736}\sum_{i=1}^{n} 2\% \cdot load(h, i) \cdot (\mathit{CO_2 \ emissions_{without \ project}(h, i)} - \mathit{CO_2 \ emissions_{with \ project}(h, i)})

    where:

    - *load(h, i)* is the load (in MW) for hour *h* and market zone *i*,
    - :math:`{CO_2 \ emissions_{without \ project}(h, i)}` are the |CO2| emissions
      (in tons / MWh) calculated without the project to assess,
    - :math:`{CO_2 \ emissions_{with \ project}(h, i)}` are the |CO2| emissions
      (in tons / MWh) calculated with the project to assess,

    With this correction, the full monetization for |CO2| due to losses will be:

    .. math::

        \mathit{CO_2 \ total \ monetization_{project \ X}} = \mathit{CO_2 \ monetization_{with \ project \ X}} - \mathit{CO_2 \ monetization_{without \ project \ X}} - \mathit{Correction}

    Annex : Some examples of societal costs:
    ----------------------------------------

    -  France `Quinet Report <https://www.strategie.gouv.fr/sites/strategie.gouv.fr/files/atoms/files/fs-2019-rapport-la-valeur-de-laction-pour-le-climat_0.pdf>`_ :

       ============  ==================
       Report        |CO2| cost in 2030
       ============  ==================
       2008 version    100 € / ton
       2018 version    250 € / ton
       ============  ==================

    - Germany `Umweltbundesamt report <https://www.umweltbundesamt.de/sites/default/files/medien/378/publikationen/hgp_umweltkosten_0.pdf>`_ :

      .. image:: images/co2_emissions_germany_report.png

    - European Investment Bank `The Economic Appraisal of Investment Projects at the EIB <https://www.eib.org/attachments/thematic/economic_appraisal_of_investment_projects_en.pdf>`_ :

      .. image:: images/co2_emissions_eib_report.png

    .. |CO2| replace:: CO\ :sub:`2`
"""
from pathlib import Path
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import re
import utils
import zipfile
import sys

import matplotlib.colors as colors
from sklearn.cluster import AgglomerativeClustering
try:
    import wx
    import wx.html
except ImportError:
    pass

try:
    import createDialogBoxes_wx
except ImportError:
    pass
import ExceptionPTDF
import batch_wx

try:
    import _version

    __version__ = _version.__version__
except:
    __version__ = "asv"
# from profile_support import profile


DIALOG_BOX_PARAMETERS = [
    {
        "text": _("Zip file containing losses results"),
        "action": ["open", {"filetypes": [("zip files", "*.zip")]}],
        "varConfig": "losses_results_filename",
        "tooltip": _("Zip file containing losses results (coming from Losses Monetization module)."),
    },
    {
        "text": _("CO2 Price (€/ton)"),
        "action": ["entry"],
        "varConfig": "co2_price",
        "default_value": "100",
        "tooltip": _("CO2 Price (€/ton)"),
    },
    {
        "text": _("File containing CO2 parameters"),
        "action": ["open", {"filetypes": [("xlsx files", "*.xlsx")]}],
        "varConfig": "co2_filename",
        "default_value": r".\data\PEMMDB_Common_Data.xls",
        "tooltip": _("File containing CO2 parameters."),
    },
    {
        "text": _("File containing merit order parameters"),
        "action": ["open", {"filetypes": [("xlsx files", "*.xlsx")]}],
        "varConfig": "merit_order_filename",
        "default_value": r".\data\CMg_TYNDP2020.xlsx",
        "tooltip": _("File containing merit order parameters."),
    },
    {
        "text": "Scenario",
        "action": [
            "combo",
            [
                "National Trends 2025",
                "National Trends 2030",
                "National Trends 2040",
                "Distributed Energy 2030",
                "Distributed Energy 2040",
                "Global Ambition 2030",
                "Global Ambition 2040",
            ],
        ],
        "varConfig": "scenario",
        "tooltip": _("Name of the scenario"),
    },
    {
        "text": _("File containing installed capacities"),
        "action": ["open", {"filetypes": [("csv files", "*.csv;*.csv.gz")]}],
        "varConfig": "installed_capacities_filename",
        "default_value": r".\data\yearly_outputs_MMStandardOutputFile_NT2025_ANTARES_CY2007_P0.csv",
        "tooltip": _("File containing installed capacities for the scenario."),
    },
    {
        "text": _("Output file: "),
        "action": [
            "save",
            {
                "filetypes": [("csv files", "*.csv;*.zip;*.gz")],
                "defaultextension": ".csv",
            },
        ],
        "help": r"help/html/losses.html#outputs",
        "tooltip": _("Name of the output file, containing CO2 monetization results."),
        "varConfig": "co2_result_filename",
    },
]


def start_batch():
    param_batch_dialogbox = [
        {
            "text": "Fichier batch",
            "action": ["open", {"filetypes": [("csv files", "*.csv")]}],
            "varConfig": "batch_filename",
            "help": "get_parameters",
        }
    ]

    batch_wx.batch(
        "CO2 emissions",
        DIALOG_BOX_PARAMETERS,
        Path(__file__).name,
        param_batch_dialogbox,
        calculate_co2_emissions,
    )


def start_ihm():
    frame = wx.Frame(None, -1)
    createDialogBoxes_wx.make_dialog(
        wx.ScrolledWindow,
        frame,
        "CO2 emissions evaluation",
        Path(__file__).name,
        DIALOG_BOX_PARAMETERS,
        calculate_co2_emissions,
        help_file=r"help\html\co2_emissions.html",
        about=("CO2 emissions", "CO2 emissions - version " + __version__),
        with_status_bar=True,
    )
    frame.Fit()
    frame.SetMinSize(frame.GetSize())
    frame.Centre()
    frame.Show()


def background_gradient(s, m, M, cmap="PuBu", low=0, high=0):
    rng = M - m
    norm = colors.Normalize(m - (rng * low), M + (rng * high))
    normed = norm(s.values)
    c = [colors.rgb2hex(x) for x in plt.cm.get_cmap(cmap)(normed)]
    return ["background-color: %s" % color for color in c]


def read_co2_parameters(co2_filename):
    # Read CO2 parameters
    co2 = pd.read_excel(co2_filename, sheet_name="Common Data", header=19, nrows=29)
    co2 = co2.dropna(subset=["Category #"]).set_index("Category #")
    co2["CO2 emission factor"] = co2["CO2 emission factor"].fillna(method="ffill")
    co2[["CO2 emission factor", "Standard efficiency in NCV terms"]] = co2[
        ["CO2 emission factor", "Standard efficiency in NCV terms"]
    ].astype(float)

    # Calculate CO2 emissions in Ton/MWh per fuel type
    co2["emission_ton_mwh"] = (
        3.6
        * co2["CO2 emission factor"]
        / co2["Standard efficiency in NCV terms"]
        / 1000
    )
    return co2


def read_merit_order_prices(merit_order_filename, scenario, co2_price):
    # Read merit order file
    merit_order = (
        pd.read_excel(merit_order_filename, header=0, index_col=0).rename(
            columns=lambda x: re.sub("\s+", " ", x)
        )
        # Suppress empty cols (labeled Unnamed)
        .pipe(utils.drop_unnamed_cols)
    )
    if scenario not in merit_order.columns:
        raise ExceptionPTDF.ExceptionPTDF(
            (
                "Error",
                "Invalid scenario name",
                "Scenario should be one of the following: "
                + ", ".join(set(merit_order.columns).difference(set(["Fuel Type"]))),
            )
        )

    merit_order = merit_order.dropna(subset=[scenario])

    # Get CO2 price used to calculate SEW
    co2_sew_price = merit_order[scenario].iat[0]
    co2_losses_price = int(co2_price) - co2_sew_price
    merit_order = merit_order.drop(merit_order.index[0])
    return merit_order, co2_losses_price


def read_installed_capacities(installed_capacities_filename):
    # Read Installed capacities file
    market_output = pd.read_csv(
        installed_capacities_filename,
        sep=";",
        encoding="latin1",
        header=5,
        index_col=1,
        decimal=",",
        thousands=" ",
    ).fillna(method="ffill")
    market_output = pd.DataFrame(
        market_output[market_output.iloc[:, 0].str.startswith("Installed Capacities")]
        .iloc[:, 1:]
        .dropna(axis=1)
        .sum(axis=1),
        columns=["Installed Capacities"],
    )
    # Keep fuel types with installed capacity greater than 1000 MW
    market_output = market_output[market_output > 1000].dropna()
    print(market_output.head(30))
    return market_output


def generate_clusters(merit_order, installed_capacities, PRICE_DIFFERENCE, scenario):
    prices = merit_order[scenario].values.reshape(-1, 1)
    clustering = AgglomerativeClustering(
        distance_threshold=PRICE_DIFFERENCE, n_clusters=None, linkage="single"
    ).fit(prices)
    merit_order["cluster"] = clustering.labels_
    merit_order["is_clustered"] = [
        (clustering.labels_ == x).sum() != 1 for x in clustering.labels_
    ]
    merit_order = pd.merge(
        installed_capacities,
        merit_order,
        right_on="Fuel Type",
        left_index=True,
        how="inner",
    )

    price_cluster = []
    emission_cluster = []
    fuel_type_cluster = []
    for idc, g in merit_order.groupby("cluster"):
        if len(g) > 1:
            # if cluster: new fuel type with price and co2 emissions weighted
            # by installed capacities of the clustered fuel type
            fuel_type_cluster.append(" + ".join(g["Fuel Type"].tolist()))
            price_cluster.append(
                g[scenario].dot(g["Installed Capacities"])
                / g["Installed Capacities"].sum()
            )
            emission_cluster.append(
                g["emission_ton_mwh"].dot(g["Installed Capacities"])
                / g["Installed Capacities"].sum()
            )
    clustered = pd.DataFrame(
        {
            "emission_ton_mwh": emission_cluster,
            "Fuel Type": fuel_type_cluster,
            scenario: price_cluster,
        }
    )
    clustered.index += 100
    merit_order = pd.concat([merit_order[~merit_order["is_clustered"]], clustered])
    return merit_order


def read_input_zipfile(losses_results_filename):
    file = {}
    with zipfile.ZipFile(losses_results_filename, "r") as losses_zipfile:
        for filename in losses_zipfile.filelist:
            arc_filename = Path(filename.filename)
            print(arc_filename)
            if arc_filename.name.startswith("mgl_cost_after"):
                file["mgl_cost_after"] = filename
            elif arc_filename.name.startswith("mgl_cost_before"):
                file["mgl_cost_before"] = filename
            elif arc_filename.name.startswith("losses_dc_per_country_after_"):
                file["dc_losses_after"] = filename
            elif arc_filename.name.startswith("losses_dc_per_country_before_"):
                file["dc_losses_before"] = filename
            elif "[losses_after]" in arc_filename.name:
                file["losses_after"] = filename
            elif "[losses_before]" in arc_filename.name:
                file["losses_before"] = filename

        mgl_cost = {}
        losses_results = {}
        dc_losses_results = {}
        mgl_costs_filename = {
            "before": file["mgl_cost_before"],
            "after": file["mgl_cost_after"],
        }
        for situation in ["before", "after"]:
            # Read the file containing marginal costs
            mgl_cost[situation] = pd.read_csv(
                losses_zipfile.open(mgl_costs_filename[situation]),
                sep=";",
                index_col=["Hour"],
            )
            # Read losses results
            losses_results[situation] = pd.read_csv(
                losses_zipfile.open(file["losses_" + situation]),
                sep=";",
                header=0,
                index_col=["Hour"],
            )
            if "dc_losses_before" in file:
                dc_losses_results[situation] = pd.read_csv(
                    losses_zipfile.open(file["dc_losses_" + situation]),
                    sep=";",
                    header=0,
                    index_col=["Hour"],
                )
                losses_results[situation] = losses_results[situation].add(
                    dc_losses_results[situation], fill_value=0
                )

        return mgl_cost, losses_results


def calculate_co2_emissions(
    losses_results_filename,
    co2_price,
    co2_filename,
    merit_order_filename,
    scenario,
    installed_capacities_filename,
    co2_result_filename,
    progressbar=None,
    status_bar=None,
    log_std=None,
    log_err=None,
    output_only=False,
):

    # Calculate CO2 emissions per fuel type using CO2 parameters file
    co2_emissions = read_co2_parameters(co2_filename)

    # Read marginal cost per fuel type
    merit_order, co2_losses_price = read_merit_order_prices(
        merit_order_filename, scenario, co2_price
    )

    merit_order = pd.merge(
        merit_order, co2_emissions, left_index=True, right_index=True
    ).sort_values(by=scenario)

    # Read installed capacities
    installed_capacities = read_installed_capacities(installed_capacities_filename)

    # Generate clusters if price difference < 2 €
    PRICE_DIFFERENCE = 2
    merit_order = generate_clusters(
        merit_order, installed_capacities, PRICE_DIFFERENCE, scenario
    )

    # Read files coming from losses monetization module
    mgl_cost, losses_results = read_input_zipfile(losses_results_filename)

    dict_market_zones = {
        "ITN1": "ITN",
        "ITS1": "ITS",
        "DKE1": "DKE",
        "DKW1": "DKW",
        "ITSA": "ITSAR",
        "ITSI": "ITSIC",
        "LUB1": "LUB",
        "LUF1": "LUF",
        "LUG1": "LUG",
        "LUV1": "LUV",
        "NOM1": "NOM",
        "NON1": "NON",
        "NOS0": "NOS",
        "UK00": "GB",
    }
    mgl_fuel_type = {}
    mgl_fuel_type_frequency = {}
    co2_emission = {}
    co2_mwh = {}
    sum_co2_emission = {}
    monetization = {}
    # Dictionnary fuel type code / fuel type description
    fuel_type = merit_order[["Fuel Type"]].T.to_dict("list")
    # Define interval for marginal prices depending on fuel types
    inter = merit_order[scenario].sort_values().rolling(2).mean().tolist() + [10000]
    inter[0] = -0.1
    for situation in ["before", "after"]:
        # Get list of market zones
        market_zones = [
            dict_market_zones.get(x, x.replace("00", ""))
            for x in mgl_cost[situation].columns
        ]
        # For every market zone, find for every hour, the fuel type corresponding to the marginal cost
        mgl_fuel_type[situation] = pd.DataFrame(columns=market_zones)
        for market_zone, mgl_cost_col in zip(market_zones, mgl_cost[situation].columns):
            mgl_fuel_type[situation][market_zone] = pd.cut(
                mgl_cost[situation][mgl_cost_col],
                bins=inter,
                labels=merit_order[scenario].sort_values().index,
            )

        # Calculate the frequency of marginal generation fuel type per market nodes
        mgl_fuel_type_frequency[situation] = (
            mgl_fuel_type[situation].apply(lambda x: x.value_counts(normalize=True)).T
        )
        mgl_fuel_type_frequency[situation].columns = [
            fuel_type[x][0] for x in mgl_fuel_type_frequency[situation].columns
        ]

        # For every marginal generation, get the corresponding CO2 emission (in ton / MWh)
        co2_mwh[situation] = mgl_fuel_type[situation].replace(
            merit_order["emission_ton_mwh"].to_dict()
        )
        # Calculate CO2 emission due to losses (in tons, for each hour)
        co2_emission[situation] = co2_mwh[situation] * losses_results[situation]
        # Calculate CO2 emission due to losses (in tons, for the whole year)
        sum_co2_emission[situation] = co2_emission[situation].sum().sum()
        # Calculate CO2 monetization due to losses (in M€, for every hour)
        monetization[situation] = co2_losses_price * co2_emission[situation] / 1_000_000

    total_emission = sum_co2_emission["after"] - sum_co2_emission["before"]

    monetization_diff = monetization["after"] - monetization["before"]
    total_monetization = monetization_diff.sum().sum()
    print(total_monetization)
    print(total_emission)
    co2_results = pd.concat(
        [monetization["after"], monetization["before"], monetization_diff],
        axis=1,
        keys=[
            "CO2 monetization After",
            "CO2 monetization Before",
            "Delta CO2 monetization [After - Before]",
        ],
    )
    co2_results.to_csv(co2_result_filename, sep=";")
    co2_result_path = Path(co2_result_filename)
    co2_result_filename_without_ext = co2_result_path.stem
    style = "<style>.dataframe td { text-align: right; }</style>"
    html_header = (
        '<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">'
        + "<head><title>"
        + co2_result_filename_without_ext
        + "</title></head>"
        + "<p><h2>Simulation : "
        + co2_result_filename_without_ext
        + "</h2><p>"
        + "<p><h3>Scenario : "
        + scenario
        + "</h3><p>"
        + "<p><h3>CO<sub>2</sub> price : "
        + co2_price
        + " € / ton"
        + "</h3><p>"
        + "<p><h2>Total CO<sub>2</sub> monetization : {:,.2f}".format(
            total_monetization
        ).replace(",", " ")
        + " M€ difference [after - before]"
        + "<p><h2>Total CO<sub>2</sub> emission : {:,.2f}".format(
            total_emission
        ).replace(",", " ")
        + " ton difference [after - before]"
        + "<p><h3>CO<sub>2</sub> emission After: {:,.2f}".format(
            sum_co2_emission["after"]
        ).replace(",", " ")
        + " ton"
        + "</h3>"
        + "<p><h3>CO<sub>2</sub> emission Before: {:,.2f}".format(
            sum_co2_emission["before"]
        ).replace(",", " ")
        + " ton"
        + "</h3><p>"
    )
    html_results = style + html_header

    pd.options.display.max_colwidth = -1

    html_results += "<p><h2>CO<sub>2</sub> monetization summary:<h2><p>"
    monetization_sum = pd.DataFrame(co2_results.sum().T)
    synthesis = pd.pivot_table(
        monetization_sum,
        columns=monetization_sum.index.get_level_values(0),
        index=monetization_sum.index.get_level_values(1),
    )
    synthesis.columns = synthesis.columns.droplevel(level=0)
    synthesis = synthesis.sort_values(by=["Delta CO2 monetization [After - Before]"])
    html_results += synthesis.to_html(float_format=lambda x: "%.1f" % x)

    html_results += "<p><h2>Marginal generation per fuel type:<h2><p>"
    cm = sns.light_palette("green", as_cmap=True)
    mgl_fuel_type_before_style = (
        mgl_fuel_type_frequency["before"]
        .style.format("{:.1%}")
        .background_gradient(cmap=cm, axis="columns")
    )
    html_results += mgl_fuel_type_before_style.render()

    html_results += (
        "<p><h2>Variation of marginal generation per fuel type [After - Before]:<h2><p>"
    )
    delta_mgl_fuel = (
        mgl_fuel_type_frequency["after"] - mgl_fuel_type_frequency["before"]
    )
    cm = sns.diverging_palette(
        10, 130, s=99, l=32, n=200, center="light", as_cmap=True
    )  # noqa
    abs_max = max([abs(delta_mgl_fuel.min().min()), abs(delta_mgl_fuel.max().max())])
    delta_mgl_fuel_type_style = delta_mgl_fuel.style.format("{:.1%}").apply(
        background_gradient, cmap=cm, m=-abs_max, M=abs_max
    )
    html_results += delta_mgl_fuel_type_style.render()

    html_results += "<p><h2>Input Files:<h2><p>"
    co2_params = pd.DataFrame(
        {
            "Inputs": [
                Path(losses_results_filename).name,
                Path(co2_filename).name,
                Path(merit_order_filename).name,
                scenario,
            ]
        },
        index=["Losses filename", "CO2 filename", "Merit order filename", "Scenario"],
    ).to_html()
    html_results += co2_params

    with open(
        co2_result_path.parent / (co2_result_filename_without_ext + "-results.html"),
        "w",
        encoding="utf8",
    ) as synthesis:
        synthesis.write(html_results)

    utils.log_or_display_msg(
        (
            "Info",
            "Results",
            f"{scenario}:{co2_result_filename_without_ext} CO2 price={co2_price}, CO2 emission={total_emission}, CO2 monetization={total_monetization}",
        ),
        log_std,
        log_err,
        output_only=output_only,
    )


if __name__ == "__main__":
    root = wx.App()
    start_ihm()
    root.MainLoop()
