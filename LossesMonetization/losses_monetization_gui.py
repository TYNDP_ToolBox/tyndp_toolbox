# vim: set fileencoding=utf-8
import wx
import losses_monetization
import ihm_main
import extract_load_ens_mglcosts
import co2_emissions

class IhmPTDFNormal(ihm_main.IhmMain):
    def __init__(self, parent):
        self.liste_modules = [
            {
                "texte": "Extract Marginal Costs from Market Outputs",
                "groupe": "TYNDP",
                "function": extract_load_ens_mglcosts.start_ihm,
            },
            {
                "texte": "Losses Monetization",
                "tooltip": (
                    "Losses monetization"
                ),
                "groupe": "TYNDP",
                "function": losses_monetization.start_ihm,
            },
            {
                "texte": "CO2 Monetization",
                "tooltip": (
                    "CO2 monetization"
                ),
                "groupe": "TYNDP",
                "function": co2_emissions.start_ihm,
            },
        ]
        ihm_main.IhmMain.__init__(
            self,
            None,
            -1,
            "Losses Toolbox : MODE NORMAL",
            "Mode NORMAL",
            help_file=r"help\html\index.html",
            liste_modules=self.liste_modules,
            liste_groupes=[
                "TYNDP",
            ],
        )


if __name__ == "__main__":
    app = wx.App()
    frame = IhmPTDFNormal(None)
    frame.Show()
    app.MainLoop()
