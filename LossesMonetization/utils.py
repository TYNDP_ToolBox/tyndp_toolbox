﻿# vim: set fileencoding=utf-8
import zipfile
from pathlib import Path
import re
import codecs
import os
import errno
import gzip
import traceback
from io import StringIO
import shutil
import time
import sys

import pandas
import numpy

try:
    import wx
    import wx.html2
    import winreg
except ImportError:
    pass
# from profile_support import profile


def isnumeric(data):
    """Test if data is a numeric value."""

    try:
        float(data.replace(",", "."))
        return True
    except (AttributeError, ValueError):
        try:
            float(data)
            return True
        except (ValueError, TypeError):
            return False


def get_decimal_sign_from_csv_file(filename, usecols, starting_row, encoding="latin1"):
    """Permet de déterminer le séparateur décimal d'un fichier csv.

    Args:
        filename (str): Nom du fichier csv
        usecols (list): liste des noms des colonnes sur lesquelles la
            détermination du séparateur décimal est effectué (l'idée est
            de passer des colonnes qui contiennent des valeurs
            numériques uniquement)
        starting_row (int): nombre de lignes à sauter après l'entete.

    Returns: (str) le séparateur décimal, '.' ou ','

    """
    # Try to find the decimal separator : ',', '.'
    with FileOrZipfile(filename, mode="r", encoding=encoding) as csv_file:
        for_decimal_sep = pandas.read_csv(
            csv_file,
            delimiter=";",
            header=0,
            encoding=get_pd_encoding(csv_file),
            usecols=usecols,
            nrows=20,
            skiprows=list(range(1, starting_row + 1)),
        )
    if "object" in for_decimal_sep.dtypes.tolist():
        decimal_sep = ","
    else:
        decimal_sep = "."
    return decimal_sep


def get_starting_row_from_csv_file(filename, usecols=None, encoding="latin1"):
    """Calcul de la ligne du début de fichier.

    Fonction permettant de connaître la première ligne non vide du fichier,
    après l'entete.

    Args:
        filename (str): nom du fichier

    Kwargs:
        usecols (list): liste des colonnes à prendre en compte.

    Returns: (int) le numéro de la première ligne non vide du fichier.

    """
    # Calculate the starting row
    with FileOrZipfile(filename, mode="r", encoding=encoding) as csv_file:
        for_starting_row = pandas.read_csv(
            csv_file,
            encoding=get_pd_encoding(csv_file),
            delimiter=";",
            header=0,
            usecols=usecols,
            nrows=20,
        )
    starting_row = for_starting_row.index.get_loc(
        for_starting_row.dropna(axis=0, how="any").index[0]
    )

    return starting_row


def check_file_for_writing(filename):
    msg = ("Info", "", "-->ok")
    caracteres_interdits = r'/:*?"\<>|'
    if any(i in os.path.basename(filename) for i in caracteres_interdits):
        msg = (
            "Error",
            _("Incorrect name of file"),
            _("Impossible to create file {}: its name containes one of the forbidden characters: {}").format(
                os.path.basename(filename), caracteres_interdits
            ),
        )
    else:
        if sys.platform == "win32":
            import win32api

            if len(filename) < 260:
                try:
                    import win32file

                    fic = win32file.CreateFile(
                        filename,
                        win32file.GENERIC_WRITE,
                        0,
                        None,
                        win32file.OPEN_ALWAYS,
                        win32file.FILE_ATTRIBUTE_NORMAL,
                        None,
                    )
                    fic.Close()
                except win32api.error as error:
                    (code, function, message) = error.args
                    msg = (
                        "Error",
                        _("Error while trying to create the file"),
                        _("Impossible to write in file [{}]: {}").format(filename, message),
                    )
                except:
                    msg = ("Error", _("Unexpected error"), traceback.format_exc())
            else:
                msg = (
                    "Error",
                    _("Error during file creation: name too long"),
                    _("Filename {filename} exceeds the max 255 characters allowed by Windows...Impossible to create."),
                )
        else:
            try:
                with open(filename, "w"):
                    pass
            except IOError as err:
                msg = (
                    "Error",
                    "Erreur d'ouverture du fichier",
                    f"Problème d'écriture dans le fichier "
                    f"[{filename}] : {err.strerror}",
                )
            except:
                msg = ("Error", "Exception imprévue", traceback.format_exc())
    return msg


def create_directory(directory):
    msg = ("", "", "")
    try:
        os.makedirs(directory)
    except OSError as exc:  # Python >2.5
        message = exc.strerror
        if exc.errno == errno.EEXIST and os.path.isdir(directory):
            pass
        else:
            msg = (
                "Error",
                _("Impossible to create folder"),
                _("Impossible to create folder [{}]: {}").format(directory, message),
            )
            return msg
    return msg


def check_file_for_opening(filename):
    msg = ("Info", "", "-->ok")
    if len(filename) < 256:
        try:
            with open(filename, "r"):
                pass
            if os.path.getsize(filename) == 0:
                msg = (
                    "Error",
                    _("Erreur opening the file (empty file)"),
                    _("The file {} is empty").format(filename),
                )
            else:
                msg = ("Info", "", "-->ok")
        except IOError:
            msg = (
                "Error",
                _("Error opening the file (non existing file)"),
                _("The file {} doesn't exist or its path is incorrect").format(filename),
            )
    else:
        msg = (
            "Error",
            _("Error while opening file: name too long"),
            _("Filename {filename} exceeds the max 255 characters allowed by Windows..."),
            "Erreur d'ouverture du fichier (nom trop long)",
        )

    return msg


def log_or_display_msg(
    exc,
    log_std=None,
    log_err=None,
    style=None,
    output_only=False,
    parent=None,
    to_status_bar=False,
):
    if not exc:
        return
    try:
        msg = exc.message
    except:
        msg = exc
    is_error = False
    # Si msg est une liste : ce sont des warnings
    if isinstance(msg, list):
        liste_warnings = [
            str(x.message).split(":", 2)
            for x in msg
            if str(x.message).startswith("WarningPTDF")
        ]
        for warning in liste_warnings:
            if warning[0].startswith("WarningPTDF"):
                warning[0] = warning[0].replace("WarningPTDF", "Warning")
                log_or_display_msg(
                    tuple(warning), log_std, log_err, style, output_only, parent
                )
        return is_error
    if isinstance(msg, tuple):
        msg_level, msg_title, msg_text = msg
    else:
        msg_level, msg_title, msg_text = msg.args[0]
    if to_status_bar:
        try:
            status_bar = parent.GetStatusBar()
            status_bar.SetStatusText(msg_text)
            wx.GetApp().Yield()
        except AttributeError:
            pass

    if output_only and not log_std:
        print(msg)
        return is_error
    if msg_level == "Error":
        is_error = True
        if log_err:
            msg_text = msg_text.replace("\n\n", "\n")
            msg_text = msg_text.replace("\n", "\n" + " " * 20)
            log(log_err, msg_level, msg_title, msg_text, style)
            log(log_std, msg_level, msg_title, msg_text, style)
        else:
            dial = wx.MessageDialog(parent, msg_text, msg_title, wx.OK | wx.ICON_ERROR)
            dial.ShowModal()
        return is_error
    elif msg_level == "Warning":
        if log_std:
            msg_text = msg_text.replace("\n\n", "\n")
            msg_text = msg_text.replace("\n", "\n" + " " * 20)
            log(log_std, msg_level, msg_title, msg_text, style)
        else:
            dial = wx.MessageDialog(
                parent, msg_text, msg_title, wx.OK | wx.ICON_EXCLAMATION
            )
            dial.ShowModal()
        return is_error
    elif msg_level == "Info":
        if log_std:
            msg_text = msg_text.replace("\n\n", "\n")
            msg_text = msg_text.replace("\n", "\n" + " " * 20)
            log(log_std, msg_level, msg_title, msg_text, style)
        else:
            dial = wx.MessageDialog(
                parent, msg_text, msg_title, wx.OK | wx.ICON_INFORMATION
            )
            dial.ShowModal()
        return is_error


def log(log_widget, type_mesg, title, message, style=None):
    try:
        if title == "":
            message_full = time.strftime(r"%Y-%m-%d %H:%M:%S") + ":" + message + "\n"
        else:
            message_full = (
                time.strftime("%Y-%m-%d %H:%M:%S") + ":" + title + ":" + message + "\n"
            )
        if isinstance(log_widget, wx.TextCtrl):
            if type_mesg == "Error":
                log_widget.SetDefaultStyle(wx.TextAttr("red"))
                log_widget.AppendText(message_full)
                log_widget.SetDefaultStyle(wx.TextAttr())
            elif type_mesg == "Warning":
                log_widget.SetDefaultStyle(wx.TextAttr("orange"))
                log_widget.AppendText(message_full)
                log_widget.SetDefaultStyle(wx.TextAttr())
            else:
                log_widget.AppendText(message_full)
            if style == "underlined":
                log(log_widget, type_mesg, "", "-" * (len(title + message) + 3))
            wx.GetApp().Yield()
        else:
            log_widget.insert(message_full)
            log_widget.see("end")
            root = log_widget.winfo_toplevel()
            root.update()
    except:
        pass


def replace_comma(x):
    """Conversion d'une chaine de caractères en réel.

    Le séparateur décimal peut être le point ou la virgule.

    Args:
        x (str ou float): la chaine de caractères à convertir.

    Returns: le réel correspondant à la chaine de caractères, NaN si
        la conversion était impossible.

    """
    try:
        return float(x.replace(",", "."))
    except AttributeError:
        return x
    except ValueError:
        return numpy.nan


def replace_comma_and_thousands_separator(x):
    """Conversion d'une chaine de caractères en réel en prenant en compte un
    éventuel séparateur des milliers.

    Le séparateur décimal peut être le point ou la virgule.

    Args:
        x (str ou float): la chaine de caractères à convertir.

    Returns: le réel correspondant à la chaine de caractères, NaN si
        la conversion était impossible.

    """
    try:
        return float(x.replace(",", ".").replace("\xa0", "").replace(" ", ""))
    except AttributeError:
        return x
    except ValueError:
        return numpy.nan


def convert_numeric(
    dataframe, type_numeric="float64", raise_exception=False, separateur_milliers=None
):
    if isinstance(dataframe, pandas.Series):
        if dataframe.dtypes == object:
            if separateur_milliers:
                dataframe_numeric = dataframe.apply(
                    replace_comma_and_thousands_separator
                )
            else:
                dataframe_numeric = dataframe.apply(replace_comma)
            return dataframe_numeric
        else:
            return dataframe
    else:
        # On cherche les colonnes texte
        non_numeric_cols = (dataframe.dtypes == object).to_numpy().nonzero()[0]
        if len(non_numeric_cols) > 0:
            # S'il en reste, on convertit le texte en réel
            dataframe_numeric = dataframe
            if separateur_milliers:
                try:
                    dataframe_numeric = dataframe.applymap(
                        replace_comma_and_thousands_separator
                    )
                except AttributeError:
                    dataframe_numeric = dataframe.apply(
                        lambda x: x.replace(",", ".").replace("\xa0", "")
                    ).convert_objects(convert_numeric=True)
                if raise_exception:
                    if numpy.all(
                        (
                            (dataframe_numeric.dtypes == numpy.float64)
                            | (dataframe_numeric.dtypes == numpy.float32)
                            | (dataframe_numeric.dtypes == numpy.int64)
                            | (dataframe_numeric.dtypes == numpy.int32)
                        )
                    ):
                        pass
                    else:
                        raise TypeError(
                            "Impossible to convert data into numeric values"
                        )
            else:
                try:
                    dataframe_numeric = dataframe.applymap(replace_comma)
                except AttributeError:
                    dataframe_numeric = dataframe.apply(
                        lambda x: x.replace(",", ".")
                    ).convert_objects(convert_numeric=True)
                if raise_exception:
                    if numpy.all(
                        (
                            (dataframe_numeric.dtypes == numpy.float64)
                            | (dataframe_numeric.dtypes == numpy.float32)
                            | (dataframe_numeric.dtypes == numpy.int64)
                            | (dataframe_numeric.dtypes == numpy.int32)
                        )
                    ):
                        pass
                    else:
                        raise TypeError(
                            "Impossible to convert data into numeric values"
                        )
            return dataframe_numeric
        else:
            # Sinon, que des valeurs numériques : rien à faire
            return dataframe


def get_compression_type(filename):
    file_path = Path(filename)
    compression = None
    extension = file_path.suffix
    if extension == ".gz":
        compression = "gzip"
    elif extension == ".bz2":
        compression = "bz2"
    elif extension == ".zip":
        compression = "zip"
    elif extension in (".snappy", ".sz"):
        compression = "snappy"
    return compression


def zip_file(filename):
    # Par defaut :
    # - le nom du fichier n'est pas modifié dans l'archive
    # - le nom de l'archive est le nom du fichier auquel on ajoute l'extension .zip

    # si le fichier contient déjà l'extension zip
    if os.path.splitext(filename)[1] == ".zip":
        # Alors le nom de l'archive sera le nom du fichier initial
        arcname = filename
        # ... et le nom du fichier dans l'archive devra contenir l'extension .csv
        if os.path.splitext(os.path.splitext(filename)[0])[1] != ".csv":
            # .. que l'on rajoute si le nom du fichier initial ne la contenait pas
            filename_in_zip = (
                os.path.splitext(os.path.splitext(filename)[0])[0] + ".csv"
            )
        else:
            filename_in_zip = os.path.splitext(filename)[0]
        # On renomme le fichier initial avec le nom qu'il prendra dans le zip
        shutil.move(filename, filename + "-tmp")

        # et on zippe
        fic_out_zippe = zipfile.ZipFile(arcname, "w", allowZip64=True)
        fic_out_zippe.write(
            filename + "-tmp",
            arcname=os.path.basename(filename_in_zip),
            compress_type=zipfile.ZIP_DEFLATED,
        )
        fic_out_zippe.close()
        os.remove(filename + "-tmp")


def remove_leading_slash(string):
    if string.startswith(("\\", "/")):
        string = string[1:]
    return string


def check_mandatory_cols(filename, mandatory_cols, encoding="latin1"):
    """Fonction permettant de vérifier que les colonnes obligatoires
    existent bien dans un fichier csv.

    Args:
        filename (str): nom du fichier csv.
        mandatory_cols (list of str): liste des colonnes à trouver dans le
            fichier.

    Kwargs:
        encoding (str): nom de l'encoding à prendre en compte dans ce fichier.

    Returns:
        (tuple): tuple contenant :
            col_names (list): le nom des colonnes contenues dans le fichier
            missing_mandatory_col(list): le nom des colonnes obligatoires,
                mais manquantes dans le fichier csv.

    """
    col_names = []
    missing_mandatory_col = []
    with FileOrZipfile(filename, mode="r", encoding=encoding) as fid:
        header = pandas.read_csv(
            fid,
            encoding=get_pd_encoding(fid),
            delimiter=";",
            header=0,
            nrows=1,
            dtype=str,
        ).pipe(drop_unnamed_cols)
    col_names = header.columns
    missing_mandatory_col = set(mandatory_cols).difference(col_names)
    return col_names, missing_mandatory_col


def drop_unnamed_cols(dataframe):
    """Supprimer les colonnes dont l'entete contient Unnamed.

    Args:
        dataframe (pd.DataFrame): le dataframe dans lequel supprimer les entetes.

    Returns: le dataframe modifié

    """
    unnamed_cols = [
        col for col in dataframe.columns if col.lower().startswith("unnamed")
    ]
    return dataframe.drop(unnamed_cols, axis=1)


def check_csv_format(filename):
    """Fonction permettant de vérifier qu'un fichier csv a été créé dans
    le bon encoding (latin1), et n'est pas au format Mac csv (MacRoman),
    ou DOS csv (encoding cp437)

    Args:
        filename (str): nom du fichier pour lequel on souhaite vérifier
            l'encoding.

    Returns: TODO

    """
    file_format = "csv"
    fid = codecs.open(filename, "r", encoding="latin1")
    text = fid.read()
    # if ['\r\n' in text]
    if [ord(c) for c in text if ord(c) >= 0x80 and ord(c) <= 0x9F]:
        file_format = "csv_dos"
    return file_format


def get_basedir(directory, filename):
    """Fonction permettant de récupérer le répertoire de base dans lequel
    vont être cherchés les autres fichiers définis dans le fichier batch
    par exemple.


    Args:
        directory (str): nom du dossier
        filename (str): nom du fichier batch (en général)

    Returns:
        basedir (str): nom du dossier de base à partir duquel seront ouverts
        les autres fichiers:
            - si directory est un dossier absolu (débute par le nom d'un
            lecteur D:, C:, K:...), retourne ce dossier absolu,
            - si directory est un dossier relatif au dossier qui contient
            le fichier filename, retourne la concaténation du dossier
            contenant le fichier filename et le directory relatif,
            - sinon, retourne une chaine de caractères vide.

    """
    directory = remove_leading_slash(directory)

    if os.path.isdir(directory) and re.match(r"[A-Za-z]:", directory):
        # Répertoire absolu (le nom du répertoire commence par un nom du
        # lecteur D:, K:, ...)
        basedir = directory
    elif os.path.isdir(os.path.join(os.path.dirname(filename), directory)):
        # Répertoire relatif au répertoire dans lequel se trouve le
        # fichier batch
        basedir = os.path.join(os.path.dirname(filename), directory)
    else:
        # Sinon pas de répertoire de base
        basedir = ""
    return basedir


def set_reg(name, reg_path, value):
    try:
        winreg.CreateKey(winreg.HKEY_CURRENT_USER, reg_path)
        registry_key = winreg.OpenKey(
            winreg.HKEY_CURRENT_USER, reg_path, 0, winreg.KEY_WRITE
        )
        winreg.SetValueEx(registry_key, name, 0, winreg.REG_DWORD, value)
        winreg.CloseKey(registry_key)
        return True
    except WindowsError:
        return False


def get_reg(name, reg_path):
    try:
        registry_key = winreg.OpenKey(
            winreg.HKEY_CURRENT_USER, reg_path, 0, winreg.KEY_READ
        )
        value, regtype = winreg.QueryValueEx(registry_key, name)
        winreg.CloseKey(registry_key)
        return value
    except WindowsError:
        return None


def display_html_file(title, html_file):
    # Bitbucket nécessite d'utiliser une version récente d'internet explorer:
    # par défaut, wxpython utilise IE7. Le code ci-dessous (qui modifie un registre Windows)
    # permet d'utiliser IE11.
    if wx.Platform == "__WXMSW__" and "bitbucket" in html_file:
        reg_path = r"Software\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION"
        set_reg(os.path.basename(sys.executable), reg_path, 11001)
        print(get_reg(os.path.basename(sys.executable), reg_path))
    help_frame = wx.Frame(None, -1, title, pos=(0, 0), size=wx.DisplaySize())
    browser = wx.html2.WebView.New(help_frame)
    browser.LoadURL(html_file)
    help_frame.Show()


def get_pd_encoding(fid):
    if not hasattr(fid, "encoding"):
        return "latin1"


class FileOrZipfile:
    def __init__(self, filename, mode="r", encoding="latin1"):
        self.filename = filename
        self.file_id = None
        self.encoding = encoding
        self.mode = mode

    def __enter__(self):
        if isinstance(self.filename, (list, pandas.DataFrame)):
            return self.filename
        if self.filename == "":
            return []
        filename = self.filename
        if self.mode == "r":
            if not isinstance(filename, StringIO):
                if Path(filename).with_suffix(".lnk").is_file():
                    import win32com.client

                    shell = win32com.client.Dispatch("WScript.Shell")
                    shortcut = shell.CreateShortCut(
                        str(Path(filename).with_suffix(".lnk"))
                    )
                    print("shortcut=", shortcut.Targetpath)
                    filename = shortcut.Targetpath

                if get_compression_type(filename) == "zip" and zipfile.is_zipfile(
                    filename
                ):
                    archive = zipfile.ZipFile(filename, "r", allowZip64=True)
                    liste_fichiers_zippes = archive.infolist()
                    self.file_id = archive.open(liste_fichiers_zippes[0].filename, "r")
                    return self.file_id
                if get_compression_type(filename) == "gzip":
                    self.file_id = gzip.open(filename, "rt", encoding=self.encoding)
                    return self.file_id
                else:
                    self.file_id = open(filename, "r", encoding=self.encoding)

                return self.file_id
            else:
                self.file_id = filename
                self.file_id.seek(0)
                return self.file_id

        elif self.mode == "w":
            try:
                compression_type = get_compression_type(filename)
                if compression_type == "gzip":
                    mode = "wt"
                    self.file_id = gzip.open(
                        filename, mode=mode, encoding=self.encoding, compresslevel=3
                    )
                else:
                    self.file_id = open(filename, "w", encoding=self.encoding)
            except (TypeError, AttributeError):
                return self.filename
            return self.file_id

    def __exit__(self, exc_ty, exc_val, t_b):
        try:
            if not isinstance(self.filename, StringIO):
                self.file_id.close()
        except AttributeError:
            pass
        self.file_id = None
