﻿# vim: set fileencoding=utf-8
"""
    .. _losses_monetization_intro:


    Losses Monetization
    ===================

    Introduction
    ------------
    The goal of this module is to calculate losses monetization for a given reinforcement.
    Knowing the losses after and before a reinforcement and marginal costs,
    it will be possible to evaluate losses monetization for all market nodes, for all the point in time.

    Losses difference
    -----------------

    Once losses are calculated with network tools, it is easy to calculate
    the impact of a reinforcement on losses, for every point in time:

    :math:`\Delta Losses_{Projet}(PiT) = \displaystyle\sum_{c \in countries} Losses_{After Projet}(c, PiT) - Losses_{Before Projet}(c, PiT)`

    Monetization
    ------------

    By using marginal costs (before and after the reinforcement) , it will be possible to
    calculate cost of losses (in €).

    :math:`losses\_costs_{Before Project}(PiT) = \displaystyle\sum_{c \in countries} Losses_{Before Project}(c, PiT) . mrgl\_cost_{Before Project}(c, PiT)`

    :math:`losses\_costs_{After Project}(PiT) = \displaystyle\sum_{c \in countries} Losses_{After Project}(c, PiT) . mrgl\_cost_{After Project}(c, PiT)`

    :math:`\Delta losses\_costs_{Project}(PiT) = losses\_costs_{After Project}(PiT) - losses\_costs_{Before Project}(PiT)`



    Input data needed
    -----------------

    Yearly losses per country
    ^^^^^^^^^^^^^^^^^^^^^^^^^

    In order to calculate (and monetize) losses difference for a reinforcement,
    yearly losses files before and after the reinforcement will be needed.

    Each of these files will contain losses for every country, for all point in times.

    .. note::
      
      Please note that since losses are computed in network tools, it is
      usually not possible to use market nodes as defined in the market
      simulations.

      In such cases, a dictionnary is build to link coutry ISO codes (as
      defined in network models) and market nodes (defined in market
      simulations):

      ============ =================
      Market Nodes Country ISO codes
      ============ =================
        ITN1         ITN
        ITS1         ITS
        DKE1         DKE
        DKW1         DKW
        ITSA         ITSAR
        ITSI         ITSIC
        LUB1         LUB
        LUF1         LUF
        LUG1         LUG
        LUV1         LUV
        NOM1         NOM
        NON1         NON
        NOS0         NOS
        UK00         GB
        XX00         XX
      ============ =================

    The fisrt column has to be named **Hour** and should contain PiT numbers,
    other columns should contain losses (in MW) for every country:

    .. image:: images/losses_monetization_losses_file.png



    DC losses [if not already included in losses calculated by network tool]
    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    .. note::

      If DC losses are not included in the Yearly losses per country files
      (depending on the network tool used), it will be necessary to compute
      them and add these DC losses to the previously given AC losses file.

    Losses in HVDC link are composed of:

        - idle losses (losses generated at P=0 MW)
        - losses proportional to the absolute value of the active power of the HVDC

    :math:`Losses_{DC}(MW) = Pnom . losses_{at P=0}(\% of Pnom) + \\lvert P \\rvert . losses_{at P=Pnom}(\% of Pnom)`

    With:

        - P: active power of the HVDC (MW)
        - Pnom: nominal active power of the HVDC (MW)
        - losses at P=0 MW: idle losses (% of Pnom)
        - losses at P=Pnom: losses proportional to active power (% of Pnom)

    HVDC setpoints
    ~~~~~~~~~~~~~~

    This file should contain HVDC setpoints for every HVDC taking part to the
    losses assessment, for every Hour.

    The format of the file should be the following:

    .. image:: images/losses_monetization_hvdcsetpoints.png

    .. warning::

      If an HVDC defined in the *HVDC characteristics* file is missing in this
      *HVDC setpoints* file, the following message will appear:

          .. code-block:: none

           Impossible to evaluate losses: Impossible to evaluate losses: The
           following HVDC are defined in the file <name_of_hvdc_file> but can't
           be found in the HVDC setpoints file <name_of_the_flow_file>:,
           followed by the missing lines

    Characteristics of HVDC links
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Each HVDC will have a different percentage of losses depending on the number
    of converter stations, the technology (LCC or VSC) the number of kilometres
    of cable, and so on.

    The calculation for HVDC losses will
    be (e.g. HVDC IFA_2000_A of the table above): :math:`0.2\% * 1000 + 2.2\% * \\lvert Flow (MW) \\rvert`

    For each HVDC taken into account, the *HVDC* file must contain:

    - **HVDC**: name of the HVDC, as defined in network model (the name should
      match the names of HVDC found in `HVDC setpoints` file),
    - **Pnom**: maximum capacity of the HVDC,
    - **% losses for P=0**: percentage of idle losses (in %)
    - **% losses for P=Pnom**: percentage of losses proportional to active power (in %)
    - **Country From**: the country from which the HVDC link is connected
    - **Country To**: the country to which the HVDC link is connected
    - **selected**: if losses for a given HVDC should be assessed, this column
      should contain **x**, **X**, or **1**. Otherwise, the HVDC will be
      ignored.

    The exact format of the *HVDC* file is the following:

    .. image:: images/HVDC_characteristics.png

    .. note::

       Losses calculated for HVDC are evenly split between Country From and Country To for every HVDC.




    Marginal costs files
    ^^^^^^^^^^^^^^^^^^^^

    In order to monetize losses, hourly marginal costs per market nodes (before
    and after the project) are needed.
    They can be extracted from market simulations, using the module
    **Extract Marginal Costs from Market Output files**.

    The format of these files should be as follows (every name of columns has
    to be followed by ``_Mgl Cost``):

    .. image:: images/losses_marginal_costs.png

    Monetization Parameters
    ^^^^^^^^^^^^^^^^^^^^^^^

    .. _mgl-cost-inv-value:

    Upper bound to consider marginal cost as invalid
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Every marginal costs greater or equal to this upper bound will be
    considered as invalid, and will be replaced by the
    :ref:`replacement value for invalid marginal cost <inv-mgl_cost-replacement-value>`.

    .. _inv-mgl_cost-replacement-value:

    Replacement value for invalid marginal costs
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    In case of energy not supplied, the value of marginal price can reach 3000
    €/MWh. In order to avoid taking this costs into account, we can define a
    replacement value for marginal costs considered as invalid, for example:

    - **0** : for hours containing at least one marginal cost equal or greater
      to :ref:`the defined upper bound <mgl-cost-inv-value>`, every marginal costs (for
      every country) will be replaced by 0 €/MWh.

      .. note::

         The idea is to ignore such hours. But in certain cases, the market
         simulation before the project contains more invalid hours than the
         market simulation after the project. A zero-value will have the effect
         of ignoring hours before the project, whereas the same hours with the
         project will be taken into account, which is maybe not the desired
         calculation.

    - **NAN** : for hours containing at least one marginal cost equal or
      greater than :ref:`the defined upper bound <mgl-cost-inv-value>`, every
      marginal costs (for every country) will be replaced by an invalid value.

      .. note::

         Using **NAN** as the replacement value will have the effect of ignoring
         hours in case of a marginal cost before OR after the project is equal
         or greater than the :ref:`the defined upper bound <mgl-cost-inv-value>`.


    .. _losses_monetization_informations:

    Informations
    ^^^^^^^^^^^^

    - **Scenario**: name of the scenario (NT2025, NT2030, ...)
    - **Project ID**: id of the project (P16, P133, ...)
    - **Market Simulator**: Antares, PowrSym, BID, Plexos, ...
    - **Climate Year**: 1982, 1984, 2007
    - **PINT / TOOT**: type of the project (PINT or TOOT)

    These informations will be used to build the name of the output file.

    .. _losses_monetization_output_files:

    Output folder
    ^^^^^^^^^^^^^

    Name of the folder in which the zip file (containing all the input and
    output files) will be created.

    Outputs
    -------
    At the end of the losses computation, a popup appears, showing one of the following message:

    - if the number of PiT is **greater** than 8700, losses in MWh can be calculated:

      .. code-block:: none

        Evaluation of losses is successful: End of losses evaluation:
        <losses> MWh difference [losses after - losses before], based on <number_of_PiT> PiT

    - if the number of PiT is **less** than 8700, losses in MWh can't be calculated without weighted the PiT:

      .. code-block:: none

        Evaluation of losses is successful: End of losses evaluation: Losses have been calculated for <number_of_PiT> PiT
        (see output file). These PiT should be weighted to have yearly losses

    The ``losses_monetization_results.zip`` file
    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    An output zip file will be created, its name will be ``losses_monetization_results_{suffix}.zip``.

    .. note::

      The ``suffix`` part is built based on the input provided:

      ``suffix = {scenario}_{CY}_{market tool}_{project id}_{PINT or TOOT}``

    The zip file will contain:

    - in the ``input_files`` folder, all the files needed to calculate losses monetization
    - in the ``output_files`` folder all files generated by the module.

    ``input_files`` folder
    ~~~~~~~~~~~~~~~~~~~~~~

      - the initial ``marginal_costs`` file (before and after the project)
      - the losses per country per hour calculated by network tools (before and after the project)
      - **if DC losses were not included** in the losses calculated by network tools:

        - HVDC setpoints files (before and after the project)
        - HVDC characteristics files (before and after the project)

    ``output_files`` folder
    ~~~~~~~~~~~~~~~~~~~~~~~

      This folder will contain the following files:


      - two ``mgl_costs`` csv files (before and after the project), containing
        initial marginal costs, **with invalid values capped**, same names as
        initial marginal costs files, except that suffix
        ``_(cost_gt_<invalid_values>_replaced_by_<replacement_values>).csv``
        will be added to their name

      - ``delta_losses_{suffix}.csv``, a file containing the differences in
        AC and DC losses (in MW) due to the project, per country and hour,

      - ``delta_losses_monetization_{suffix}.csv``, a file containing the differences in term of
        losses monetization (in M€) due to the project, per country and hour,

      - ``synthesis_{suffix}.html``, a summary of the losses calculation is written in an html file:

        - the total difference of losses between the two situations, for example:

          .. image:: images/losses_summary.png

        - the losses by country, for example:

          .. image:: images/losses_summary_by_country.png

        - the result of losses monetization

          .. image:: images/losses_summary_monetization.png

      If DC losses needed to be calculated (**if they were not already included in
      losses** calculated by network tools), more files will be added to the zip
      file:

      - ``Diff_HVDC_params_{suffix}.html``, an html file containing differences
        between HVDC Before and After

        To avoid mistakes during the losses evaluation process, an html file will
        be generated, showing differences between the two files, before and after
        reinforcement (HVDC added / deleted, changes in losses parameters)

        .. image:: images/losses_output_diffHVDC.png
        |

      - ``losses_dc_per_country_before_{suffix}.csv`` and
        ``losses_dc_per_country_after_{suffix}.csv``, two csv files containing DC losses calculated 
"""
from itertools import zip_longest
import os
import io
from pathlib import Path
import math
import traceback
import difflib
import gzip
import zipfile
import pandas as pd
import numpy as np

try:
    import wx
    import wx.html
except ImportError:
    pass

import utils

try:
    import createDialogBoxes_wx
except ImportError:
    pass
import batch_wx
import ExceptionPTDF

try:
    import _version

    __version__ = _version.__version__
except:
    __version__ = "asv"
# from profile_support import profile

DIALOG_BOX_PARAMETERS = [
    {
        "separator": "Input Files BEFORE reinforcement",
        "text": "File containing losses per country results BEFORE reinforcement",
        "action": ["open", {"filetypes": [("csv files", "*.csv;*.zip;*.gz")]}],
        "varConfig": "losses_per_country_before_filename",
        "tooltip": (
            "File containing hourly losses per country BEFORE the reinforcement."
        ),
        "help": r"help/losses_monetization_losses_file.png",
    },
    {
        "text": "File containing HVDC setpoints BEFORE reinforcement [if not already included in losses file]",
        "action": [
            "open",
            {"filetypes": [("csv files", "*.csv;*.zip;*.gz")]},
            "optional",
        ],
        "varConfig": "hvdc_flow_before_filename",
        "tooltip": (
            "File containing hourly flows for every HVDC BEFORE the reinforcement."
        ),
        "help": r"help/losses_monetization_hvdcsetpoints.png",
    },
    {
        "text": (
            "File containing losses parameters for HVDC BEFORE "
            "reinforcement [if needed]"
        ),
        "action": ["open", {"filetypes": [("csv files", "*.csv")]}, "optional"],
        "varConfig": "HVDC_characteristics_before_filename",
        "tooltip": (
            "File containing losses parameters for HVDC BEFORE the " "reinforcement."
        ),
        "help": r"help/HVDC_characteristics.png",
    },
    {
        "text": "File containing marginal costs per country BEFORE",
        "action": [
            "open",
            {"filetypes": [("csv files", "*.csv;*.csv.gz")]},
            "optional",
        ],
        "varConfig": "mgl_cost_before_filename",
        "tooltip": (
            "File containing marginal costs per country BEFORE the reinforcement."
        ),
        "help": r"help/losses_marginal_costs.png",
    },
    {
        "separator": "Input Files AFTER reinforcement",
        "text": "File containing losses per country results AFTER reinforcement [if not already included in losses file]",
        "action": ["open", {"filetypes": [("csv files", "*.csv;*.zip;*.gz")]}],
        "varConfig": "losses_per_country_after_filename",
        "tooltip": (
            "File containing hourly losses per country AFTER the reinforcement."
        ),
        "help": r"help/losses_monetization_losses_file.png",
    },
    {
        "text": "File containing HVDC setpoints AFTER reinforcement",
        "action": [
            "open",
            {"filetypes": [("csv files", "*.csv;*.zip;*.gz")]},
            "optional",
        ],
        "varConfig": "hvdc_flow_after_filename",
        "tooltip": (
            "File containing hourly flows for every HVDC AFTER the reinforcement."
        ),
        "help": r"help/losses_monetization_hvdcsetpoints.png",
    },
    {
        "text": (
            "File containing losses parameters for HVDC AFTER "
            "reinforcement [if needed]"
        ),
        "action": ["open", {"filetypes": [("csv files", "*.csv")]}, "optional"],
        "varConfig": "HVDC_characteristics_after_filename",
        "tooltip": (
            "File containing losses parameters for HVDC AFTER the reinforcement."
        ),
        "help": r"help/HVDC_characteristics.png",
    },
    {
        "text": "File containing marginal costs per country AFTER",
        "action": [
            "open",
            {"filetypes": [("csv files", "*.csv;*.csv.gz")]},
            "optional",
        ],
        "varConfig": "mgl_cost_after_filename",
        "tooltip": (
            "File containing marginal costs per country AFTER the reinforcement."
        ),
        "help": r"help/losses_marginal_costs.png",
    },
    {
        "separator": "Monetization Parameters",
        "text": "Marginal costs greater or equal to this value will be considered as invalid",
        "action": ["entry"],
        "varConfig": "mgl_cost_inv_upper_bound",
        "help": r"help/html/losses_monetization.html#mgl-cost-inv-value",
        "tooltip": (
            "Every hour containing marginal cost greater or equal to "
            "this value will be replaced by the mgl_cost_replacement_value defined."
        ),
    },
    {
        "text": "Replacement value for invalid marginal costs",
        "action": ["entry"],
        "varConfig": "mgl_cost_replace_inv_values",
        "tooltip": (
            "Marginal price that will replace invalid values (NAN to "
            "ignore invalid values before AND after the project)."
        ),
        "help": r"help/html/losses_monetization.html#inv-mgl-cost-replacement-value",
    },
    {
        "separator": "Information",
        "text": "Scenario",
        "action": ["entry"],
        "varConfig": "scenario",
        "tooltip": ("Name of scenario",),
        "help": r"help/html/losses_monetization.html#informations",
    },
    {
        "text": "Project ID",
        "action": ["entry"],
        "varConfig": "project_name",
        "tooltip": ("Project ID",),
        "help": r"help/html/losses_monetization.html#informations",
    },
    {
        "text": "Market simulator",
        "action": ["entry"],
        "varConfig": "market_simulator",
        "tooltip": ("Market simulator: Antares, PowrSym, Pymas, ...",),
        "help": r"help/html/losses_monetization.html#informations",
    },
    {
        "text": "Climate Year",
        "action": ["combo", ["2007", "1984", "1982"]],
        "varConfig": "climate_year",
        "tooltip": ("Market simulator: Antares, PowrSym, Pymas, ...",),
        "help": r"help/html/losses_monetization.html#informations",
    },
    {
        "text": "PINT/TOOT",
        "action": ["combo", ["PINT", "TOOT"]],
        "varConfig": "pint_or_toot",
        "tooltip": ("Market simulator: Antares, PowrSym, Pymas, ...",),
        "help": r"help/html/losses_monetization.html#informations",
    },
    {
        "separator": "Ouputs",
        "text": "Output folder : ",
        "action": [
            "openDir",
        ],
        "help": r"help/html/losses.html#outputs",
        "tooltip": "Name of the output folder, which will contain losses results.",
        "varConfig": "losses_result_folder",
    },
]


def read_HVDC_parameters(hvdc_filename):
    """Read HVDC configuration file (name, Pnom, idle losses, losses at Pnom)

       Args:
           hvdc_filename (str): name of the HVDC configuration file

       Returns: dict containing all the HVDC informations

    """
    hvdcs = pd.DataFrame([], columns=["Pnom", "idle_losses", "Pnom_losses"])
    if hvdc_filename:
        col_names, _ = utils.check_mandatory_cols(hvdc_filename, [], encoding="latin1")
        usecols = col_names[0:4].tolist()
        if len(usecols) >= 4:
            names = {
                x: y
                for x, y in zip(
                    col_names, ["Name", "Pnom", "idle_losses", "Pnom_losses"]
                )
            }
            usecols = usecols + ["Country From", "Country To"]
            if "selected" in col_names:
                usecols = usecols + ["selected"]
            with utils.FileOrZipfile(hvdc_filename) as hvdc_file:
                hvdcs = pd.read_csv(
                    hvdc_file,
                    delimiter=";",
                    encoding=utils.get_pd_encoding(hvdc_file),
                    header=0,
                    index_col=0,
                    usecols=usecols,
                ).rename(columns=names)
            hvdcs = hvdcs[pd.notnull(hvdcs.index)]
            hvdc_duplicated = hvdcs.index.duplicated()
            if hvdc_duplicated.any():
                msg = (
                    "Error",
                    "Error: Duplicate HVDC in the file",
                    "The following HVDC are duplicated in the file "
                    "{filename} : {dup_hvdc}".format(
                        filename=hvdc_filename,
                        dup_hvdc=", ".join(list(hvdcs.index[hvdc_duplicated])),
                    ),
                )
                raise ExceptionPTDF.ExceptionPTDF(msg)
            if "selected" in hvdcs.columns:
                hvdcs = hvdcs[hvdcs["selected"].isin(["x", "X", 1])]
                hvdcs = hvdcs.drop(labels=["selected"], axis=1)
            try:
                hvdcs["Country To"] = hvdcs["Country To"].str.upper()
                hvdcs["Country From"] = hvdcs["Country From"].str.upper()
            except:
                pass
            hvdcs["Pnom"] = utils.convert_numeric(hvdcs["Pnom"])
            hvdcs["idle_losses"] = (
                utils.convert_numeric(hvdcs["idle_losses"]).fillna(0) / 100.0
            )
            hvdcs["Pnom_losses"] = (
                utils.convert_numeric(hvdcs["Pnom_losses"]).fillna(0) / 100.0
            )

    return hvdcs


def start_batch():
    param_batch_dialogbox = [
        {
            "text": "Fichier batch",
            "action": ["open", {"filetypes": [("csv files", "*.csv")]}],
            "varConfig": "batch_filename",
            "help": "get_parameters",
        }
    ]

    batch_wx.batch(
        "Batch Losses Monetization calculation",
        DIALOG_BOX_PARAMETERS,
        os.path.basename(__file__),
        param_batch_dialogbox,
        calculate_losses_monetization,
    )


def start_ihm():
    frame = wx.Frame(None, -1)
    createDialogBoxes_wx.make_dialog(
        wx.ScrolledWindow,
        frame,
        "Losses monetization evaluation",
        os.path.basename(__file__),
        DIALOG_BOX_PARAMETERS,
        calculate_losses_monetization,
        help_file=r"help\html\losses.html",
        about=(
            "Losses Monetization Evaluation",
            "Losses Monetization Evaluation - version " + __version__,
        ),
        with_status_bar=True,
    )
    frame.Fit()
    frame.SetMinSize(frame.GetSize())
    frame.Centre()
    frame.Show()


def read_mgl_costs_file(
    lite_hourly_data, mgl_cost_invalid_values, mgl_cost_replacement_value
):
    mgl_cost_replacement_value = utils.replace_comma(mgl_cost_replacement_value)
    mgl_cost_invalid_values = utils.replace_comma(mgl_cost_invalid_values)
    mgl_costs = lite_hourly_data.filter(like="COST").rename(
        columns=lambda x: x.replace("_MGL COST", "").upper()
    )
    if math.isnan(mgl_cost_replacement_value):
        mgl_costs[mgl_costs >= mgl_cost_invalid_values] = float("nan")
        mgl_costs[pd.isnull(mgl_costs).any(axis="columns")] = mgl_cost_replacement_value
    else:
        mgl_costs[mgl_costs >= mgl_cost_invalid_values] = mgl_cost_replacement_value
    return mgl_costs


# @profile
def calculate_losses_monetization(
    losses_per_country_before_filename,
    hvdc_flow_before_filename,
    HVDC_characteristics_before_filename,
    mgl_cost_before_filename,
    losses_per_country_after_filename,
    hvdc_flow_after_filename,
    HVDC_characteristics_after_filename,
    mgl_cost_after_filename,
    mgl_cost_inv_upper_bound,
    mgl_cost_replace_inv_values,
    scenario,
    project_name,
    market_simulator,
    climate_year,
    pint_or_toot,
    losses_result_folder,
    progressbar=None,
    status_bar=None,
    log_std=None,
    log_err=None,
    output_only=False,
):
    # Create output dir if doesn't exist
    msg = utils.create_directory(losses_result_folder)
    if msg[0] == 'Error':
        utils.log_or_display_msg(msg)
        return msg

    input_files = [
        {"name": losses_per_country_before_filename, "suffix": "[losses_before].csv"},
        {"name": mgl_cost_before_filename},
        {"name": losses_per_country_after_filename, "suffix": "[losses_after].csv"},
        {"name": mgl_cost_after_filename},
    ]
    # Check HVDC files
    hvdc_files = {
        "File containing HVDC setpoints after the reinforcement": hvdc_flow_after_filename,
        "File containing HVDC setpoints before the reinforcement": hvdc_flow_before_filename,
        "File containing HVDC losses settings before the reinforcement": HVDC_characteristics_before_filename,
        "File containing HVDC losses settings after the reinforcement": HVDC_characteristics_after_filename,
    }
    # If all HVDC files (needed to compute losses for HVDC) exist, then dc losses will be computed
    if all(hvdc_files[v] != "" for v in hvdc_files):
        calculate_dc_losses = True
        input_files += [
            {"name": hvdc_flow_before_filename},
            {"name": hvdc_flow_after_filename},
            {"name": HVDC_characteristics_before_filename},
            {"name": HVDC_characteristics_after_filename},
        ]
    else:
        calculate_dc_losses = False
        if any(hvdc_files[v] != "" for v in hvdc_files):
            # If at least one of the HVDC files is filled in, but not the other, error message
            missing_files = [v for v in hvdc_files if hvdc_files[v] == ""]
            msg = (
                "Error",
                "Missing HVDC files",
                f"It seems you want to calculate HVDC losses, but the following files are missing:\n"
                f"{', '.join(missing_files)}",
            )
            raise ExceptionPTDF.ExceptionPTDF(msg)
    losses_dc_per_country = {"after": 0, "before": 0}
    try:
        status_bar.SetStatusText("")
        parent = status_bar.GetParent()
    except:
        parent = None

    # Check if marginal costs invalid values and replacement values are numerical values
    msg = ""
    if not utils.isnumeric(mgl_cost_replace_inv_values):
        msg += "The replacement value for invalid marginal costs [mgl_cost_replace_inv_values] has to be defined as a numeric value.\n"
    if not utils.isnumeric(mgl_cost_inv_upper_bound):
        msg += "The upper bound defining invalid value for marginal costs [mgl_cost_inv_upper_bound] has to be defined as a numeric value.\n"
    if msg != "":
        raise ExceptionPTDF.ExceptionPTDF(
            "Error",
            "Marginal costs filenames are defined, but incorrect settings detected",
            "Marginal costs filenames are defined, but incorrect settings detected:\n"
            + msg,
        )

    # Read losses before the project
    losses_filenames = {
        "after": losses_per_country_after_filename,
        "before": losses_per_country_before_filename,
    }
    mgl_cost_filename = {
        "before": mgl_cost_before_filename,
        "after": mgl_cost_after_filename,
    }
    losses = {}
    mgl_costs = {}
    costs = {}
    country = {
        "ITN1": "ITN",
        "ITS1": "ITS",
        "DKE1": "DKE",
        "DKW1": "DKW",
        "ITSA": "ITSAR",
        "ITSI": "ITSIC",
        "LUB1": "LUB",
        "LUF1": "LUF",
        "LUG1": "LUG",
        "LUV1": "LUV",
        "NOM1": "NOM",
        "NON1": "NON",
        "NOS0": "NOS",
        "UK00": "GB",
    }
    for situation in ["after", "before"]:
        dec = utils.get_decimal_sign_from_csv_file(
            losses_filenames[situation],
            usecols=lambda x: x != "Hour",
            starting_row=0,
        )
        losses[situation] = pd.read_csv(
            losses_filenames[situation], sep=";", index_col=["Hour"], decimal=dec
        ).rename(columns=str.upper)
        mgl_costs[situation] = pd.read_csv(
            mgl_cost_filename[situation],
            sep=";",
            nrows=8736,
            usecols=lambda x: x.endswith(("Hour", "Cost")),
        ).rename(columns=str.upper)
        if "Hour" in mgl_costs[situation].columns:
            mgl_costs[situation] = mgl_costs[situation].set_index(["Hour"])
        else:
            mgl_costs[situation].index = losses[situation].index
        costs[situation] = read_mgl_costs_file(
            mgl_costs[situation],
            mgl_cost_inv_upper_bound,
            mgl_cost_replace_inv_values,
        )
        costs[situation] = costs[situation].rename(columns=country).rename(columns=lambda x: x.replace("00", ""))
        if costs[situation].empty:
            msg = (
                "Error",
                "Invalid marginal cost file",
                "The marginal cost filename {filename} must contain columns ending with '_Mgl Cost'. Impossible to evaluate losses monetization.".format(
                    filename=mgl_cost_filename[situation]
                ),
            )
            raise ExceptionPTDF.ExceptionPTDF(msg)

    # Check that we have the same market nodes
    if losses["after"].columns.tolist() != losses["before"].columns.tolist():
        raise ExceptionPTDF.ExceptionPTDF(
            "Error",
            "Inconsistent names of countries",
            "Not the same countries in the losses files, before and after the project.",
        )

    if len(losses["before"]) != len(losses["after"]):
        msg = (
            "Error",
            "Input losses files with different number of hours",
            "Number of hours of flow files are different: {nb_before} PiT before / {nb_after} PiT after.\n"
            "Impossible to evaluate losses.".format(
                nb_before=len(losses["after"]), nb_after=len(losses["before"])
            ),
        )
        raise ExceptionPTDF.ExceptionPTDF(msg)

    if calculate_dc_losses:
        hvdc_filenames = {
            "after": hvdc_flow_after_filename,
            "before": hvdc_flow_before_filename,
        }
        hvdc_settings_filenames = {
            "after": HVDC_characteristics_after_filename,
            "before": HVDC_characteristics_before_filename,
        }
        hvdc_settings = {}
        hvdc_flows = {}
        losses_dc = {}
        for situation in ["after", "before"]:
            # Read HVDC flows
            dec = utils.get_decimal_sign_from_csv_file(
                losses_filenames[situation],
                usecols=lambda x: x != "Hour",
                starting_row=0,
            )
            hvdc_flows[situation] = pd.read_csv(
                hvdc_filenames[situation], sep=";", index_col=["Hour"], decimal=dec
            )
            # Read HVDC settings
            hvdc_settings[situation] = read_HVDC_parameters(
                hvdc_settings_filenames[situation]
            )
            # If an HVDC is defined in the HVDC settings file, it has to be defined in
            # the flow file
            flows_undef = set(hvdc_settings[situation].index).difference(
                set(hvdc_flows[situation].columns)
            )
            if flows_undef:
                msg = (
                    "Error",
                    "Impossible to evaluate losses",
                    "Impossible to evaluate losses: The following HVDC are "
                    "defined in the file {hvdc_settings} but can't be found in "
                    "the HVDC setpoints file {calcflow}: {undefined_hvdc}".format(
                        hvdc_settings=os.path.basename(hvdc_settings[situation]),
                        calcflow=os.path.basename(hvdc_flows[situation]),
                        undefined_hvdc=", ".join(sorted(flows_undef)),
                    ),
                )
                utils.log_or_display_msg(
                    msg, log_std, log_err, parent=parent, output_only=output_only
                )
                return msg

            # Evaluate HVDC losses before
            losses_dc[situation] = hvdc_settings[situation][
                "idle_losses"
            ] * hvdc_settings[situation]["Pnom"] + hvdc_settings[situation][
                "Pnom_losses"
            ] * np.fabs(
                hvdc_flows[situation][hvdc_settings[situation].index]
            )

            losses_dc_per_country[situation] = (
                (
                    losses_dc[situation]
                    .T.join(
                        hvdc_settings[situation][["Country From", "Country To"]],
                        how="inner",
                    )
                    .groupby(by="Country From")
                    .sum()
                    .add(
                        losses_dc[situation]
                        .T.join(
                            hvdc_settings[situation][["Country From", "Country To"]],
                            how="inner",
                        )
                        .groupby(by="Country To")
                        .sum(),
                        fill_value=0,
                    )
                )
                / 2
            ).T
            losses_dc_per_country[situation].index.name = "Hour"
            losses[situation] = losses[situation].add(
                losses_dc_per_country[situation], fill_value=0
            )

        # Create html file showing differences between the HVDC settings file before
        # and after reinforcement
        fic_hvdc_diff = io.StringIO()
        fic_hvdc_diff.writelines(
            difflib.HtmlDiff().make_file(
                [
                    line
                    + "\t"
                    + "\t".join(
                        str(hvdc_settings["before"].loc[line, col])
                        for col in hvdc_settings["before"].columns
                    )
                    for line in sorted(hvdc_settings["before"].index)
                ],
                [
                    line
                    + "\t"
                    + "\t".join(
                        str(hvdc_settings["after"].loc[line, col])
                        for col in hvdc_settings["after"].columns
                    )
                    for line in sorted(hvdc_settings["after"].index)
                ],
                context=False,
            )
        )
    delta_losses = losses["after"].sub(losses["before"], fill_value=0)

    losses_monetization = {}
    for situation in ["after", "before"]:
        losses_monetization[situation] = costs[situation].multiply(losses[situation])

    delta_losses_monetization = (
        losses_monetization["after"] - losses_monetization["before"]
    )
    print(delta_losses_monetization.head())
    suffix = (
        "_"
        + scenario
        + "_"
        + climate_year
        + "_"
        + market_simulator
        + "_"
        + project_name
        + "_"
        + pint_or_toot
    )
    full_losses_result_filename = Path(losses_result_folder) / ("losses_monetization_results" + suffix + ".zip")
    losses = pd.concat(
        [losses["after"].sum(), losses["before"].sum(), delta_losses.sum()],
        axis=1,
        keys=["Losses After", "Losses Before", "Losses [After - Before]"],
    )
    monetization = pd.concat(
        [
            losses_monetization["after"].sum(),
            losses_monetization["before"].sum(),
            delta_losses_monetization.sum(),
        ],
        axis=1,
        keys=[
            "Costs After",
            "Costs Before",
            "Costs [After - Before]",
        ],
        sort=True,
    )

    style = "<style>.dataframe td { text-align: right; }</style>"
    nb_pit = len(delta_losses)
    delta_total_losses = delta_losses.sum().sum()
    html_header = (
        '<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">'
        + "<head><title>"
        + Path(full_losses_result_filename).stem
        + "</title></head>"
        + "<p><h2>Simulation : "
        + Path(full_losses_result_filename).stem
        + "</h2><p>"
        + "<p><h2>Total losses : {:,.0f}".format(delta_total_losses).replace(",", " ")
        + " MWh difference [losses after - losses before], based on "
        + "%d" % len(delta_losses)
        + " Hours"
        + "</h2><p>"
    )
    html_results = "<h2>Losses by country (MWh)</h2><p>" + losses.sort_values(
        by="Losses [After - Before]"
    ).to_html(justify="right", classes="", float_format=lambda x: "%.f" % x)
    costs_results = pd.DataFrame(delta_losses_monetization.sum()).unstack().T
    html_results += (
        "<p><h2>Total costs : {:,.0f}".format(costs_results.sum()).replace(",", " ")
        + " € difference [costs after - costs before]</h2><p>"
    )

    # Add a message in the html file to recap the way high marginal
    # costs are handled
    if mgl_cost_replace_inv_values != "NAN":
        html_results += (
            "<h4>NB: Marginal costs greater than {bound} "
            "€ are replaced with {replace_cost} €.</h4><p>".format(
                bound=mgl_cost_inv_upper_bound, replace_cost=mgl_cost_replace_inv_values
            )
        )
    else:
        html_results += (
            "<h4>NB: All hours containing at least one "
            "marginal cost greater than {bound} € are "
            "ignored before AND after the project.</h4><p>".format(
                bound=mgl_cost_inv_upper_bound
            )
        )
    html_results += "<h2>Costs by country (€)</h2><p>" + (
        monetization.sort_values(by="Costs [After - Before]").to_html(
            justify="right", classes="", float_format=lambda x: "%.f" % x
        )
    )

    html_results += "<h2>Input files:</h2><p>"
    pd.options.display.max_colwidth = -1
    html_files = f"""<table border="1">
                    <thead>
                       <tr>
                          <th>Before</th>
                          <th>After</th>
                       </tr>
                    </thead>
                    <tbody>
                       <tr>
                          <td title="{str(Path(losses_per_country_before_filename).parent)}">{Path(losses_per_country_before_filename).name}</td>
                          <td title="{str(Path(losses_per_country_after_filename).parent)}">{Path(losses_per_country_after_filename).name}</td>
                       </tr>
                       <tr>
                          <td title="{str(Path(mgl_cost_before_filename).parent)}">{Path(mgl_cost_before_filename).name}</td>
                          <td title="{str(Path(mgl_cost_after_filename).parent)}">{Path(mgl_cost_after_filename).name}</td>
                       </tr>
                    </tbody>
                    </table>"""
    html_results = style + html_header + html_results + html_files
    with zipfile.ZipFile(
        full_losses_result_filename,
        mode="w",
        allowZip64=True,
        compression=zipfile.ZIP_DEFLATED,
    ) as merged_file:

        # Save input files in zip
        for input_file in input_files:
            if input_file["name"].endswith("gz"):
                merged_file.writestr(
                    os.path.join("input_files", Path(input_file["name"]).name.replace(".gz", "") + input_file.get("suffix", "")),
                    data=gzip.open(input_file["name"], "r").read(),
                    compress_type=zipfile.ZIP_DEFLATED,
                )
            else:
                merged_file.write(
                    input_file["name"],
                    arcname=Path("input_files")
                    / (
                        Path(input_file["name"]).name.replace(".csv", "")
                        + input_file.get("suffix", "")
                    ),
                    compress_type=zipfile.ZIP_DEFLATED,
                )

        # Save output files in zip
        output_files = [
            {
                "name": Path("output_files") / (f"delta_losses{suffix}.csv"),
                "content": delta_losses,
            },
            {
                "name": Path("output_files")
                / (f"delta_losses_monetization{suffix}.csv"),
                "content": delta_losses_monetization,
            },
            {
                "name": Path("output_files")
                / (
                    f"mgl_cost_after_(cost_gt_{mgl_cost_inv_upper_bound}_replaced_by_{mgl_cost_replace_inv_values}).csv"
                ),
                "content": costs["after"],
            },
            {
                "name": Path("output_files")
                / (
                    f"mgl_cost_before_(cost_gt_{mgl_cost_inv_upper_bound}_replaced_by_{mgl_cost_replace_inv_values}).csv"
                ),
                "content": costs["before"],
            },
        ]
        if calculate_dc_losses:
            output_files += [
                {
                    "name": Path("output_files")
                    / (f"losses_dc_per_country_after{suffix}.csv"),
                    "content": losses_dc_per_country["after"],
                },
                {
                    "name": Path("output_files")
                    / (f"losses_dc_per_country_before{suffix}.csv"),
                    "content": losses_dc_per_country["before"],
                },
            ]

        for output_file in output_files:
            csv_file = io.StringIO()
            output_file["content"].to_csv(
                csv_file, sep=";", index_label=output_file["content"].index.name
            )
            csv_file.seek(0)
            merged_file.writestr(
                str(output_file["name"]),
                csv_file.getvalue(),
                compress_type=zipfile.ZIP_DEFLATED,
            )

        merged_file.writestr(
            str(Path("output_files") / ("synthesis_" + suffix + ".html")), html_results
        )
        if calculate_dc_losses:
            fic_hvdc_diff.seek(0)
            merged_file.writestr(
                str(Path("output_files") / ("Diff_HVDC_params_" + suffix + ".html")),
                fic_hvdc_diff.getvalue(),
            )

    if nb_pit < 8700:
        msg = (
            "Info",
            "Evaluation of losses is successful",
            "End of losses evaluation: Losses have been calculated for "
            "{nb_pit} PiT (see output file). These PiT should be weighted "
            "to have yearly losses".format(nb_pit=nb_pit),
        )
        utils.log_or_display_msg(
            msg, log_std, log_err, parent=parent, output_only=output_only
        )
    else:
        msg = (
            "Info",
            "Evaluation of losses is successful",
            "End of losses evaluation: {delta:.2f} MWh difference "
            "[losses after - losses before], based on {nb_pit} PiT".format(
                delta=delta_total_losses, nb_pit=nb_pit
            ),
        )
        utils.log_or_display_msg(
            msg, log_std, log_err, parent=parent, output_only=output_only
        )

    return msg


if __name__ == "__main__":
    root = wx.App()
    start_ihm()
    root.MainLoop()
