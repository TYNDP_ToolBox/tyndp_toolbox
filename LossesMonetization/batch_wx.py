# vim: set fileencoding=utf-8
import tempfile
import threading
import traceback
import os
import platform

import pandas as pd

try:
    import wx
except ImportError:
    pass

try:
    import createDialogBoxes_wx
except ImportError:
    pass
import utils
import ExceptionPTDF

try:
    import _version

    __version__ = _version.__version__
except ImportError:
    __version__ = "asv"

try:
    # Définition de l'événement émis à chaque ligne du batch exécutée
    MY_EVT_TESTDONE = wx.NewEventType()
    EVT_TESTDONE = wx.PyEventBinder(MY_EVT_TESTDONE, 1)

    # Définition de l'événement émis si une exception est émise lors de l'exécution
    # du batch
    MY_EVT_EXCEPTION = wx.NewEventType()
    EVT_EXCEPTION = wx.PyEventBinder(MY_EVT_EXCEPTION, 1)

    # Définition de l'événement émis quand tous les calculs du batch ont été
    # effectués
    MY_EVT_ALLDONE = wx.NewEventType()
    EVT_ALLDONE = wx.PyEventBinder(MY_EVT_ALLDONE, 1)

    class SendEventMsg(wx.PyCommandEvent):

        """Classe permettant d'envoyer un message de la thread vers la tâche
        principale (MainLoop wx)"""

        def __init__(self, etype, eid, msg=None, msg_detailed=None):
            """Constructeur de la classe

            Args:
                etype (TODO): TODO
                eid (TODO): TODO

            Keyword Args:
                msg (TODO): TODO
                msg_detailed (TODO): TODO

            """
            wx.PyCommandEvent.__init__(self, etype, eid)

            self._msg = msg
            self._msg_detailed = msg_detailed

        def get_msg(self):
            """Retourne la chaîne de caractère (message) envoyé par l'événement
            Returns: un message textuel

            """
            return self._msg

        def get_msg_detailed(self):
            """Retourne la chaîne de caractère (message détaillé) envoyé par l'événement
            Returns: le message textuel détaillé

            """
            return self._msg_detailed


except NameError:
    pass


class RunBatchLinesInThread(threading.Thread):

    """Classe permettant de lancer le batch dans une thread séparée de la
    thread principale, pour garder une IHM toujours active meême pendant les
    calculs."""

    def __init__(
        self,
        start_function,
        data_batch,
        batch_filename,
        params,
        log_std,
        log_err,
        output_only,
        check_only,
        parent,
    ):
        """Constructeur de la classe

        Args:
            start_function (TODO): TODO
            data_batch (TODO): TODO
            batch_filename (TODO): TODO
            params (TODO): TODO
            log_std (TODO): TODO
            log_err (TODO): TODO
            output_only (TODO): TODO
            check_only (TODO): TODO
            parent (TODO): TODO


        """
        threading.Thread.__init__(self)

        self._start_function = start_function
        self._data_batch = data_batch
        self._batch_filename = batch_filename
        self._params = params
        self._log_std = log_std
        self._log_err = log_err
        self._output_only = output_only
        self._check_only = check_only
        self._parent = parent
        self.msg_calcflow = []
        self.start()

    def run(self):
        run_batch_lines(
            self._start_function,
            self._data_batch,
            self._batch_filename,
            self._params,
            self._log_std,
            self._log_err,
            self._output_only,
            self._check_only,
            self._parent,
        )


class batch:

    """Classe permettant le lancement des modules en mode batch. """

    def __init__(
        self,
        title,
        parameters,
        module_name,
        parametres_batch,
        start_function,
        batch_filename="",
    ):
        """Constructeur de la classe batch

        Args:
            title (str): Titre de l'IHM
            parameters (list): liste des paramètres définissant l'IHM en mode
                normal
            module_name (str): nom du module exécuté en mode batch
            parametres_batch (list): liste des paramètres définissant l'IHM
                en mode batch
            start_function (fonction): nom de la fonction à appeler lors du
                clic sur OK

        Kwargs:
            batch_filename (str): utilisé dans le cadre des tests unitaires


        """
        self._title = title
        self._parameters = parameters
        self._module_name = module_name
        self._parametres_batch = parametres_batch
        self._start_function = start_function
        self._batch_filename = batch_filename
        if self._batch_filename != "":
            self._log_std = None
            self._log_err = None
            self._ihm = None
            return
        frame = wx.Frame(None, wx.ID_ANY)
        self.frame = frame

        # On connecte les différents signaux reçus de la thread 'calcul de
        # batch' aux fonctions adéquates
        self.frame.Bind(EVT_EXCEPTION, self.exception_evt)
        self.frame.Bind(EVT_ALLDONE, self.all_lines_done_evt)
        self.frame.Bind(EVT_TESTDONE, self.single_line_done_evt)

        panel = wx.Panel(frame, wx.ID_ANY)
        sizer = wx.FlexGridSizer(3, 1, 0, 0)
        sb0 = wx.StaticBox(panel, label="Fichier batch")
        sb0.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD))
        staticboxsizer0 = wx.StaticBoxSizer(sb0, wx.VERTICAL)

        self._ihm = createDialogBoxes_wx.make_dialog(
            wx.Panel,
            parent=panel,
            title=title,
            module_name="_batch_" + module_name,
            parameters=self._parametres_batch,
            start_function=self.check_batch,
            help_file=r"help\html\mode_batch.html",
            with_status_bar=True,
            mode="batch",
        )
        self._ihm.parameters = self._parameters
        staticboxsizer0.Add(self._ihm, 1, wx.EXPAND, 5)

        staticbox = wx.StaticBox(panel, label="Sortie standard")
        staticbox.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD))
        staticboxsizer = wx.StaticBoxSizer(staticbox, wx.VERTICAL)
        self._log_std = wx.TextCtrl(
            panel, style=wx.TE_MULTILINE | wx.TE_READONLY | wx.TE_RICH2, size=(900, 200)
        )
        font0 = wx.Font(10, wx.MODERN, wx.NORMAL, wx.NORMAL, False, "Consolas")
        self._log_std.SetFont(font0)
        staticboxsizer.Add(self._log_std, 1, wx.EXPAND, 5)

        sb2 = wx.StaticBox(panel, label="Sortie erreur")
        sb2.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD))
        staticboxsizer2 = wx.StaticBoxSizer(sb2, wx.VERTICAL)
        self._log_err = wx.TextCtrl(
            panel, style=wx.TE_MULTILINE | wx.TE_READONLY | wx.TE_RICH2, size=(900, 100)
        )
        font1 = wx.Font(10, wx.MODERN, wx.NORMAL, wx.NORMAL, False, "Consolas")
        self._log_err.SetFont(font1)
        staticboxsizer2.Add(self._log_err, 1, wx.EXPAND, 5)

        # sizer.Add((10, 10), 0)
        sizer.Add(staticboxsizer0, 0, wx.EXPAND, 5)
        sizer.Add(staticboxsizer, 1, wx.EXPAND, 5)
        sizer.Add(staticboxsizer2, 1, wx.EXPAND, 5)
        panel.SetSizerAndFit(sizer)
        sizer.AddGrowableCol(0, 1)
        sizer.AddGrowableRow(1, 1)
        sizer.AddGrowableRow(2, 1)
        # hbox.Add(sizer, proportion=1, flag=wx.ALL|wx.EXPAND, border=15)

        frame.Fit()
        frame.SetMinSize(frame.GetSize())
        frame.Centre()
        frame.Show()

    def batch_ended(self):
        # On rend les boutons Check / Calculs / Fermer actifs
        self._ihm.pbOk.Enable()
        self._ihm.pbCancel.Enable()
        self._ihm.pbCheck.Enable()
        # Possibilité de fermer à nouveau la fenêtre du batch (rendu inactif
        # tant que le calcul n'a pas été fini)
        self._ihm.frame.Bind(
            wx.EVT_CLOSE,
            lambda evt, enable_close=True: self._ihm.on_close(evt, enable_close),
        )

        # Si une fenêtre de log existe, on la sauvegarde dans un fichier
        # de log
        if self._log_std and not self._check_only:
            log_filename = self._batch_filename + ".log"
            with open(log_filename, "a", encoding="utf8") as log_file:
                log_file.writelines(self._log_std.GetValue())

    def exception_evt(self, event):
        """Fonction appelée si une exception est émise par le thread dans
        lequel sont effectués les calculs du batch.

        Args:
            event : événement contenant message et message détaillé

        """
        # Récupération des messages émis par l'événement
        error_log = event.get_msg()
        error_log_detailed = event.get_msg_detailed()
        utils.log_or_display_msg(
            ("Error", "Exception", error_log_detailed), self._log_std, self._log_err
        )
        self.batch_ended()

        # Sauvegarde de l'erreur dans le presse-papier
        clipdata = wx.TextDataObject()
        clipdata.SetText(error_log_detailed)
        wx.TheClipboard.Open()
        wx.TheClipboard.SetData(clipdata)
        wx.TheClipboard.Close()

        # Sauvegarde d'un fichier de log de l'erreur dans le dossier temp
        # D:\users\<user_name>\AppData\Local\bug_PTDF_xxxxx.log
        try:
            bug_report_filename = tempfile.mkstemp(prefix="bug_PTDF_", suffix=".log")
            with open(bug_report_filename[1], "w", encoding="utf8") as bug_file:
                bug_file.writelines(error_log_detailed)
        except:
            pass

        # Affichage d'une boite de dialogue contenant l'erreur
        dlg = wx.GenericMessageDialog(
            None,
            "Une erreur inattendue s'est produite :",
            f"Erreur inattendue (PTDF - Version { __version__})",
            wx.OK | wx.CANCEL | wx.ICON_ERROR,
        )
        dlg.SetExtendedMessage(error_log)
        dlg.SetBackgroundColour(wx.WHITE)
        dlg.SetOKCancelLabels("Saisir le bug dans GitLab", "Fermer")
        result = dlg.ShowModal()
        if result == wx.ID_OK:
            utils.display_html_file(
                "Report Bug on GitLab",
                "https://devin-source.rte-france.com/devstudio/PTDF/issues/new?issue[assignee_id]=&issue[milestone_id]=",
            )
        dlg.Destroy()

    def single_line_done_evt(self, event):
        """Fonction appelée à chaque fois qu'une ligne du batch a été exécutée
        par le thread dans lequel est effectué le calcul du batch.

        Args:
            event : événement contenant le message à émettre au thread principal.

        """
        try:
            msg = event.get_msg()
            self._ihm.status_bar.SetStatusText(msg)
        except AttributeError:
            pass

    def all_lines_done_evt(self, event):
        """Fonction appelée quand toutes les lignes du batch ont été exécutées
        par le thread dans lequel est effectué le calcul du batch.

        Args:
            event : événement contenant le message à émettre au thread principal.

        """
        msg = ("Info", "", "")
        utils.log_or_display_msg(msg, self._log_std, self._log_err)
        msg = event.get_msg()
        utils.log_or_display_msg(msg, self._log_std, self._log_err)

        self.batch_ended()

    def check_batch(
        self, batch_filename, check_only=False, output_only=False, status_bar=None
    ):
        """Fonction permettant de vérifier l'existence de chacun des fichiers
        définis dans le fichier batch.

        Args:
            batch_filename (str): nom du fichier de batch

        Kwargs:
            output_only (bool): True si l'on souhaite seulement une sortie
            sur la sortie standard
            status_bar : argument inutilisé mais nécessaire lors de l'appel
                à la fonction check_batch dans createDialogBoxes

        Returns: (list) liste des messages résultats du module appelé

        """
        self._batch_filename = batch_filename
        (_, _, release, _, _, _) = platform.uname()
        msg_calcflow = []
        log_std = self._log_std
        log_err = self._log_err
        self._check_only = check_only
        try:
            log_std.Clear()
            log_err.Clear()
        except:
            pass
        try:
            status_bar.SetStatusText("")
            wx.GetApp().Yield()
        except AttributeError:
            pass
        with utils.FileOrZipfile(batch_filename, "r", encoding="latin1") as batch_file:
            try:
                data_batch = (
                    pd.read_csv(
                        batch_file,
                        delimiter=";",
                        header=None,
                        encoding=utils.get_pd_encoding(batch_file),
                        dtype=str,
                        index_col=False,
                        nrows=1,
                    )
                    .dropna(how="all", axis=0)
                    .dropna(how="all", axis=1)
                )
            except pd.errors.ParserError as e:
                err = str(e).replace("\n", "")
                msg = (
                    "Error",
                    "Format incorrect pour le fichier batch",
                    f"Le fichier batch a un format incorrect (est-ce vraiment "
                    f'un fichier csv ?): "{err}"',
                )
                utils.log_or_display_msg(msg, log_std, log_err, output_only=output_only)
                self.batch_ended()
                return

        colonnes_batch = data_batch.loc[0, :].tolist()
        liste_parameters = [param["varConfig"] for param in self._parameters]
        liste_parameters_deprecated = [
            param["varConfig"]
            for param in self._parameters
            if param.get("deprecated", False)
        ]
        liste_parameters_mandatory = [
            param["varConfig"]
            for param in self._parameters
            if (("deprecated" not in param) and ("optionnel" not in param))
            or (
                ("deprecated" in param and not param["deprecated"])
                and ("optionnel" in param and not param["optionnel"])
            )
        ]
        # Warning si des paramètres obsolètes ont été trouvés dans le fichier de batch
        deprecated_parameters_in_batch = set(colonnes_batch).intersection(
            set(liste_parameters_deprecated)
        )
        if deprecated_parameters_in_batch:
            list_param_obsoletes = ", ".join(list(deprecated_parameters_in_batch))
            if self._ihm:
                self._ihm.show_file_template_help(
                    None,
                    None,
                    self._parametres_batch[0],
                    f"Attention : votre fichier batch utilise des paramètres obsolètes [<b>{list_param_obsoletes}</b>].",
                )
            msg = (
                "Warning",
                "Format incorrect pour le fichier batch",
                f"Le fichier batch utilise des paramètres obsolètes [{list_param_obsoletes}]",
            )
            utils.log_or_display_msg(msg, log_std, log_err, output_only=output_only)

        missing_mandatory_parameters = set(liste_parameters_mandatory).difference(
            set(colonnes_batch)
        )
        if missing_mandatory_parameters:
            if self._ihm:
                self._ihm.show_file_template_help(
                    None,
                    None,
                    self._parametres_batch[0],
                    "Attention : des paramètres obligatoires sont inexistants dans votre fichier batch [<b>{missing_mandatory_parameters}</b>].".format(
                        missing_mandatory_parameters=", ".join(
                            list(missing_mandatory_parameters)
                        )
                    ),
                )
            msg = (
                "Error",
                "Format incorrect pour le fichier batch : paramètres obligatoires manquants",
                "Les paramètres obligatoires suivants sont introuvables dans le fichier batch :"
                + ", ".join(list(missing_mandatory_parameters)),
            )
            utils.log_or_display_msg(msg, log_std, log_err, output_only=output_only)
            self.batch_ended()
            return

        usecols = list(set(colonnes_batch).intersection(set(liste_parameters)))
        if "repertoire_de_base" in colonnes_batch:
            usecols = ["repertoire_de_base"] + usecols

        with utils.FileOrZipfile(batch_filename, "r", encoding="latin1") as batch_file:
            data_batch = (
                pd.read_csv(
                    batch_file,
                    delimiter=";",
                    header=0,
                    encoding=utils.get_pd_encoding(batch_file),
                    dtype=str,
                    index_col=False,
                    usecols=usecols,
                )
                .dropna(how="all", axis=0)
                .fillna("")
            )
        comments = data_batch.iloc[:, 0].str.contains(r"^\s*#")
        data_batch = data_batch[~comments]

        # Traitement des paramètres optionnels, non définis dans le fichier
        # batch : on les initialise à ''
        for param in liste_parameters:
            if param not in data_batch.columns:
                data_batch[param] = ""

        fichiers_sortie = set([])
        fichiers_entree = set([])
        fichiers_sortie_doublons = set([])
        # Premier passage pour détecter fichiers de sortie en doublon
        # ou fichiers d'entrée qui sont aussi des fichiers de sortie
        for row in data_batch.index.values:
            if "repertoire_de_base" in data_batch.columns:
                basedir = utils.get_basedir(
                    data_batch.loc[row, "repertoire_de_base"], batch_filename
                )
            else:
                # Sinon pas de répertoire de base
                basedir = ""
            for param in self._parameters:
                if param["action"][0] in ("open", "save"):
                    fichier = data_batch.loc[row, param["varConfig"]]
                    fichier = utils.remove_leading_slash(fichier)
                    fichier_full = os.path.join(basedir, fichier)
                    if fichier != "":
                        if param["action"][0] == "open":
                            fichiers_entree.add(fichier_full)
                        elif param["action"][0] == "save":
                            if fichier_full in fichiers_sortie:
                                fichiers_sortie_doublons.add(fichier_full)
                            else:
                                fichiers_sortie.add(fichier_full)

        if fichiers_sortie_doublons:
            msg_err = (
                "Error",
                "Fichiers de sortie en doublon",
                "Les fichiers de sortie suivants sont en doublon dans "
                "le fichier batch: {files}".format(
                    files=", ".join(list(fichiers_sortie_doublons))
                ),
            )
            utils.log(log_err, msg_err[0], msg_err[1], msg_err[2])
            utils.log(log_std, "Error", "", "--> NOK")
            self.batch_ended()
            return msg_err
        common_files = fichiers_sortie & fichiers_entree
        if common_files:
            msg_err = (
                "Error",
                "Des fichiers de sortie et d'entrée portent le même nom",
                "Les fichiers de sortie suivants sont également définis "
                "comme des noms de fichiers d'entrée : {files}".format(
                    files=", ".join(list(common_files))
                ),
            )
            utils.log(log_err, msg_err[0], msg_err[1], msg_err[2])
            utils.log(log_std, "Error", "", "--> NOK")
            self.batch_ended()
            return msg_err

        # A chaque nouveau lancement de batch, on écrit la version PTDF
        # utilisée, avec des * comme séparateurs pour faciliter la recherche
        # dans le fichier de log si besoin
        utils.log(log_std, "Info", "", len("Version PTDF : " + __version__) * "*")
        utils.log(log_std, "Info", "", "Version PTDF : " + __version__)
        utils.log(log_std, "Info", "", len("Version PTDF : " + __version__) * "*")
        try:
            # On lance l'exécution du batch dans un thread (pour ne pas bloquer
            # l'IHM)
            RunBatchLinesInThread(
                self._start_function,
                data_batch,
                batch_filename,
                self._parameters,
                log_std,
                log_err,
                output_only,
                check_only,
                self.frame,
            )
        except AttributeError:
            # Pour les tests unitaires, pas de thread, pour pouvoir récupérer
            # le status de chaque ligne du batch
            msg_calcflow = run_batch_lines(
                self._start_function,
                data_batch,
                batch_filename,
                self._parameters,
                log_std,
                log_err,
                output_only,
                check_only,
            )

        return msg_calcflow


def run_batch_lines(
    start_function,
    data_batch,
    batch_filename,
    params,
    log_std,
    log_err,
    output_only,
    check_only,
    parent=None,
):
    msg_calcflow = []
    try:
        liste_parameters = [param["varConfig"] for param in params]
        nb_calculs = len(data_batch.index)
        nb_calculs_effectues = 0
        nb_calculs_ok = 0
        for row in data_batch.index.values:
            utils.log(
                log_std,
                "Info",
                "Reading batch file",
                f"Ligne {row + 2} :  start processing",
                style="underlined",
            )
            ligne_ok = True
            if "repertoire_de_base" in data_batch.columns:
                basedir = utils.get_basedir(
                    data_batch.loc[row, "repertoire_de_base"], batch_filename
                )
            else:
                # Sinon pas de répertoire de base
                basedir = ""
            for param in params:
                if param["action"][0] in ("open", "save"):
                    fichier = data_batch.loc[row, param["varConfig"]]
                    fichier = utils.remove_leading_slash(fichier)
                    fichier_full = os.path.join(basedir, fichier)
                    if fichier != "":
                        msg = ("", "", "")
                        if param["action"][0] == "open":
                            utils.log(
                                log_std,
                                "Info",
                                "",
                                "Test if file exists "
                                + param["varConfig"]
                                + " ("
                                + fichier_full
                                + ")",
                            )
                            msg = utils.check_file_for_opening(fichier_full)
                        elif param["action"][0] == "save":
                            # Si pas d'extension, on ajoute l'extension .csv.gz
                            # par défaut
                            if not os.path.splitext(fichier_full)[1]:
                                data_batch.loc[row, param["varConfig"]] = (
                                    data_batch.loc[row, param["varConfig"]] + ".csv.gz"
                                )
                                fichier_full = fichier_full + ".csv.gz"
                            utils.log(
                                log_std,
                                "Info",
                                "",
                                "Test if file is writeable "
                                + param["varConfig"]
                                + " ("
                                + fichier_full
                                + ")",
                            )
                            msg = utils.check_file_for_writing(fichier_full)
                        if msg[0] == "Error":
                            ligne_ok = False
                            utils.log(
                                log_err,
                                msg[0],
                                msg[1],
                                "Line "
                                + str(row + 2)
                                + " of batch file: "
                                + msg[2],
                            )
                            utils.log(log_std, "Error", "", "--> NOK")
                            msg_err = msg
                        else:
                            utils.log(log_std, msg[0], msg[1], msg[2])

                    # Sinon, si le paramètre est vide et obligatoire : erreur
                    elif (
                        "optional" not in param["action"]
                        and "optional_for_batch" not in param["action"]
                    ):
                        ligne_ok = False
                        utils.log(
                            log_err,
                            "Error",
                            "Nae of file is empty",
                            "Line {no_ligne} of batch file: "
                            "the mandatory parameter {param} is empty".format(
                                no_ligne=row + 2, param=param["varConfig"]
                            ),
                        )
                        utils.log(log_std, "Error", "", "--> NOK")
                        msg_err = msg

            if ligne_ok:
                nb_calculs_ok += 1
                if basedir == "":
                    parameters = {
                        x: y
                        for x, y in zip(
                            liste_parameters,
                            [data_batch.loc[row, param] for param in liste_parameters],
                        )
                    }
                else:
                    parameters = {
                        x: y
                        for x, y in zip(
                            liste_parameters,
                            [
                                os.path.join(
                                    basedir,
                                    utils.remove_leading_slash(
                                        data_batch.loc[row, param]
                                    ),
                                )
                                if params[liste_parameters.index(param)]["action"][0]
                                in ("save", "open", "openDir")
                                and data_batch.loc[row, param] != ""
                                else data_batch.loc[row, param]
                                for param in liste_parameters
                            ],
                        )
                    }
                parameters["log_std"] = log_std
                parameters["log_err"] = log_err
                parameters["output_only"] = output_only
                utils.log(log_std, "Info", "", "--- Run calculation")
                if not check_only:
                    try:
                        msg = "Calculations done: {no} / {nb}".format(
                            no=nb_calculs_effectues, nb=nb_calculs
                        )
                        if parent is not None:
                            evt = SendEventMsg(MY_EVT_TESTDONE, -1, msg)
                            wx.PostEvent(parent, evt)
                        msg = start_function(**parameters)
                    except ExceptionPTDF.ExceptionPTDF as exc:
                        msg = exc.args[0]
                        print(msg)
                        utils.log_or_display_msg(
                            msg, log_std, log_err, output_only=output_only
                        )
                    msg_calcflow.append(msg)
            else:
                status = {"msg": msg_err}
                msg_calcflow.append(status)
            nb_calculs_effectues += 1

        if check_only:
            last_msg = (
                "Info",
                "END of check batch file",
                " %d / %d correct lines" % (nb_calculs_ok, nb_calculs),
            )
        else:
            msg = "Calcultations done: {no} / {nb}".format(
                no=nb_calculs_effectues, nb=nb_calculs
            )
            if parent is not None:
                evt = SendEventMsg(MY_EVT_TESTDONE, -1, msg)
                wx.PostEvent(parent, evt)
            last_msg = ("Info", "END of batch", "All calculataions are done")

        # Envoi de l'événement fin du batch au thread principal
        if parent is not None:
            evt = SendEventMsg(MY_EVT_ALLDONE, -1, last_msg)
            wx.PostEvent(parent, evt)
    except:
        # Erreur inattendue : on envoie l'event EXCEPTION pour afficher le
        # message de l'erreur dans une boite de dialogue, dans le presse-papier
        # et dans un fichier de log.
        erreur = traceback.format_exc()
        error_log = "Unexpected error (TYNDPToolBox - Version {ver})\n\n" "{err}".format(
            ver=__version__, err=erreur
        )
        try:
            error_log_detailed = (
                "Unexpected error (TYNDPToolBox - Version {ver})\n\n"
                "{params}\n\n"
                "{err}".format(
                    ver=__version__,
                    params=repr(parameters).replace(",", "\n"),
                    err=erreur,
                )
            )
        except UnboundLocalError:
            error_log_detailed = error_log
        if parent is not None:
            evt = SendEventMsg(MY_EVT_EXCEPTION, -1, error_log, error_log_detailed)
            wx.PostEvent(parent, evt)

    return msg_calcflow
