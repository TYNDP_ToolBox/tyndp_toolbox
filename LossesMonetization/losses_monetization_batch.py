# vim: set fileencoding=utf-8
"""
    .. _batch_intro:

    ##########
    Batch Mode
    ##########

    ************
    Introduction
    ************

    The batch mode offers the ability to run multiple calculations, for a given
    module, with the help of a csv file defining the parameters to take into
    account.

    In every module, the :guilabel:`check_batch` button will test the existence
    of input files (and the ability to write output files), without running the
    module itself, the goal beeing to have an exhaustiv list of non existent or
    already opened files before running the batch.


    ************************************
    Batch configuration file description
    ************************************

    The content of this csv file relies on the following principles:

    - if a line starts with a ``#``, it will be considered as a comment and will be ignored,
    - the columns ``repertoire_de_base`` (base_folder) can be used in any module, with the following principles:

      - if ``repertoire_de_base`` is empty or doesn't exist in the file, the
        other columns defining input or output files are supposed to contain
        absolute path (complete path of the files has to be defined),
      - if  ``repertoire_de_base`` is defined and starts with a D: or a C: (or
        any other letter defining a disk drive, meaning absolute path), the other columns defining
        input or output files are supposed to be defined relatively to this
        starting path,

      - otherwise, the ``repertoire_de_base`` columns will be handled as a
        relative path, starting from the folder containing the batch
        configuration file.

        .. note::
            Here are a few examples for possible values for ``repertoire_de_base`` columns:

            - the dot (``.``) defines the current folder, that means the folder
              containing the batch configuration file,
              dossier dans lequel se trouve le fichier csv de batch,
            - two dots (``..``) can be used to go to the parent folder:
              ``..\A\B.csv`` will access to the file ``B.csv`` contained in the
              folder ``A``, parent of the current folder
            - to access to the file ``B.csv`` contained in a sub folder ``A``
              of the current folder: ``A\B.csv``.

      .. note::
         In case of one of the expected columns would be missing, a message will show up, saying:

         ``The Header of this batch file must contain the following columns:``, followed by the expected columns.

         By clicking on the :guilabel:`Copy \u279C Clipboard` button, it will
         be possible to copy the expected format of the configuration file in
         the ClipBoard and then copy its content in Excel.

         This popup will also appear by clicking on the :guilabel:`Fichier batch` label in the gui.

    ************
    Output files
    ************

    The ouput files created in batch mode will depend on the used module. For
    further information, you can refer to the documentation of the correponding
    module.

    .. note::

       At the end of the batch, a **log file** is created, in order to save all
       the log generated during the execution.

       The name of this log file will be the same as the batch file, except
       that the extension will be changed (**.log** instead of .csv).
"""
import wx

import ihm_main
import co2_emissions
import extract_load_ens_mglcosts
import losses_monetization


class IhmPTDFBatch(ihm_main.IhmMain):
    def __init__(self, parent):
        self.liste_modules = [
            {
                "texte": "Extract Marginal Costs from Market Outputs",
                "groupe": "TYNDP",
                "function": extract_load_ens_mglcosts.start_batch,
            },
            {
                "texte": "Losses Monetization",
                "tooltip": (
                    "Losses monetization"
                ),
                "groupe": "TYNDP",
                "function": losses_monetization.start_batch,
            },
            {
                "texte": "CO2 Monetization",
                "tooltip": (
                    "CO2 monetization"
                ),
                "groupe": "TYNDP",
                "function": co2_emissions.start_batch,
            },
        ]
        ihm_main.IhmMain.__init__(
            self,
            None,
            -1,
            "Losses Toolbox : MODE BATCH",
            "Mode BATCH",
            help_file=r"help\html\mode_batch.html",
            liste_modules=self.liste_modules,
            liste_groupes=[
                "TYNDP",
            ],
        )


if __name__ == "__main__":
    app = wx.App()
    frame = IhmPTDFBatch(None)
    frame.Show()
    app.MainLoop()
