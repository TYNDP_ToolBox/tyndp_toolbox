# vim: set fileencoding=utf-8
import sys
import os
import ctypes

import wx
import pandas as pd

import utils
import _version

__version__ = _version.__version__


class IhmMain(wx.Frame):
    def __init__(
        self, parent, id, title, mode, help_file, liste_modules, liste_groupes=[""]
    ):
        try:
            ctypes.windll.shcore.SetProcessDpiAwareness(True)
        except:
            pass
        wx.Frame.__init__(
            self, parent, id, title + " [" + __version__.split("-")[0] + "]"
        )
        panel = wx.Panel(self)
        self.help_file = help_file
        self.changelog_file = r"help\html\whatsnew.html"
        self.gitlab_file = r"help\html\bug_evolution.html"
        if hasattr(sys, "frozen"):
            self.current_dir = os.path.dirname(sys.executable)
        else:
            self.current_dir = os.path.dirname(os.path.realpath(__file__))

        icon = wx.Icon(
            os.path.join(self.current_dir, "winPython.ico"), wx.BITMAP_TYPE_ICO
        )
        self.SetIcon(icon)
        self.Bind(wx.EVT_CLOSE, self.on_close)

        menubar = wx.MenuBar()
        file_menu = wx.Menu()
        fitem = file_menu.Append(-1, "Principles", "Principles")
        self.Bind(wx.EVT_MENU, self.onDisplayHelp, fitem)
        file_menu.AppendSeparator()
        changelog_item = file_menu.Append(-1, "Change Log", "Change Log")
        self.Bind(wx.EVT_MENU, self.on_display_changelog, changelog_item)
        file_menu.AppendSeparator()
        gitlab_item = file_menu.Append(
            -1, "Bug report / Evolution request", "Bug report / Evolution request"
        )
        self.Bind(wx.EVT_MENU, self.on_display_gitlab, gitlab_item)
        file_menu.AppendSeparator()
        about_item = file_menu.Append(-1, "About", "About")
        self.Bind(wx.EVT_MENU, self.on_about_click, about_item)
        version_item = file_menu.Append(-1, "Packages version", "Version")
        self.Bind(wx.EVT_MENU, self.on_version_click, version_item)
        menubar.Append(file_menu, "&Help")
        self.SetMenuBar(menubar)

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        sbox = wx.StaticBox(panel, -1, mode)
        sbox.SetFont(wx.Font(12, wx.SWISS, wx.NORMAL, wx.BOLD))
        vbox = wx.StaticBoxSizer(sbox, wx.VERTICAL)
        for groupe in liste_groupes:
            box = wx.StaticBox(panel, -1, groupe)
            box.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD))
            font = wx.SystemSettings.GetFont(wx.SYS_DEFAULT_GUI_FONT)
            vbox_tmp = wx.StaticBoxSizer(box, wx.VERTICAL)

            for module in liste_modules:
                if module.get("show", True):
                    if "groupe" not in module or module["groupe"] == groupe:
                        button = wx.Button(
                            panel, -1, label=module["texte"], size=(600, -1)
                        )
                        button.SetFont(font)
                        try:
                            button.SetToolTip(wx.ToolTip(module["tooltip"]))
                        except:
                            pass
                        button.Bind(
                            wx.EVT_BUTTON,
                            lambda evt, y=module["function"]: self.choix_bouton(evt, y),
                        )
                        vbox_tmp.Add(
                            button, 0, wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 15
                        )
                        vbox_tmp.Add((5, 4))
            vbox.Add(vbox_tmp, 0, wx.ALL, 1)

        hbox.Add(vbox, flag=wx.ALL, border=10)
        panel.SetSizer(hbox)
        hbox.Fit(self)
        self.SetMinSize(self.GetSize())
        self.SetMaxSize(self.GetSize())
        self.Centre()

    def on_close(self, Event):
        self.Destroy()

    def choix_bouton(self, Event, action):
        state = wx.GetMouseState()
        try:
            action(show_hidden_fields=state.AltDown())
        except TypeError:
            action()

    def on_about_click(self, event):
        import numpy

        dial = wx.MessageDialog(
            None,
            "TYNDP ToolBox - version {version}\n"
            "(numpy : {np_ver} / pandas : {pd_ver})".format(
                version=__version__, np_ver=numpy.__version__, pd_ver=pd.__version__
            ),
            "TYNDP ToolBox",
            wx.OK | wx.ICON_INFORMATION,
        )
        dial.ShowModal()

    def on_version_click(self, event):
        frame = wx.Frame(None, -1, title="Version des packages utilisés")

        # Add a panel so it looks the correct on all platforms
        panel = wx.Panel(frame, wx.ID_ANY)
        style = wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL
        log = wx.TextCtrl(panel, wx.ID_ANY, size=(300, 100), style=style)
        font0 = wx.Font(10, wx.MODERN, wx.NORMAL, wx.NORMAL, False, "Consolas")
        log.SetFont(font0)

        # Add widgets to a sizer
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(log, 1, wx.ALL | wx.EXPAND, 5)
        panel.SetSizer(sizer)

        from contextlib import redirect_stdout
        import io

        f = io.StringIO()
        with redirect_stdout(f):
            print(
                """
                  ****************************************
                  Version des packages utilisés par pandas
                  ****************************************
                  """
            )
            print(pd.show_versions())
            print(
                """
                  **************************************
                  Version de tous les packages installés
                  **************************************
                  """
            )
            import _libs_version

            print(_libs_version.__version__)

        log.AppendText(f.getvalue())

        frame.Show()
        frame.Centre()

    def onDisplayHelp(self, event):
        utils.display_html_file("Help", os.path.join(self.current_dir, self.help_file))

    def on_display_changelog(self, event):
        utils.display_html_file(
            "Change Log", os.path.join(self.current_dir, self.changelog_file)
        )

    def on_display_gitlab(self, event):
        utils.display_html_file(
            "Reporting Bug / Evolution request",
            os.path.join(self.current_dir, self.gitlab_file),
        )
