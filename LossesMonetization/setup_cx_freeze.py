# vim: set fileencoding=utf-8
import sys
from pathlib import Path
import subprocess
import getpass
import shutil

from cx_Freeze import setup, Executable

VERSION_PY = """
# This file is originally generated from Git information by running 'setup.py
# version'. Distribution tarballs contain a pre-generated copy of this file.
__version__ = '%s'
"""

LIBS_VERSION_PY = """
__libs_version__ = '%s'
"""


def update_version_py():
    """Récupère le numéro de version Git et l'écrit dans un fichier _version.py
    Returns: le numéro de version Git

    """
    # Get Git version
    try:
        version = subprocess.check_output(
            r"D:\Users\guyfab\Documents\Portable\Git\bin\git.exe describe --tags --dirty --always --long",
            universal_newlines=True,
        ).rstrip()
    except EnvironmentError:
        print("unable to run git, leaving _version.py alone")
        return
    print(version)

    # Ecriture du fichier contenant la version Git
    f = open("_version.py", "w")
    f.write(VERSION_PY % version)
    f.close()
    print(f"set _version.py to '{version}'")
    return "-".join(version.split("-")[0:2])


def update_packages_versions():
    """Ecriture du fichier contenant la liste des versions des packages utilisés.
    """
    # Ecriture du fichier contenant la liste des versions des librairies
    # utilisées
    f2 = open("_libs_version.py", "w")
    f2.write('__version__ = """')
    reqs = subprocess.check_output(
        f"{sys.executable} -m pip freeze", universal_newlines=True
    )
    f2.write(reqs)
    f2.write('"""')
    f2.close()


# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {
    # "packages": ["numpy", "lxml", "bokeh.core", "pkg_resources"],
    "packages": [
        "numpy",
        "matplotlib.backends.backend_wxagg",
        "pkg_resources",
        "pandas._libs.skiplist",
        "scipy",
        "seaborn",
        "asyncio",
    ],
    "include_files": ["winPython.ico", "help", "data"],
    "excludes": [
        "PIL",
        "PyQt5",
        "sqlite3",
        "Cython",
        "argcomplete",
        "_ssl",
        "numarray",
        "notebook",
        "babel",
        "pandas.tests",
        "pyreadline",
        "doctest",
        "pydoc_data",
        "gobject",
        "glib",
        "tkinter",
        "matplotlib.backends.backend_tkagg",
        "Tkconstants",
        "Tkinter",
        "optparse",
        "IPython",
        "gtk",
        "bs4",
        "boto",
        "statsmodels",
        "sphinx",
        "patsy",
        "tornado",
        "nose",
        "pygments",
        "lib2to3",
        "babel",
        "cffi",
        "cryptography",
        "curses",
        "sphinx_rtd_theme",
        "alabaster",
        "sqlalchemy",
        "scipy.spatial.cKDTree",
    ],
}

ver = update_version_py()
update_packages_versions()

user = getpass.getuser()
dist_dir = (
    Path(fr"D:\users\{user}\AppData\Local\TYNDP_ToolBox\TYNDP_ToolBox_{ver}") / f"TYNDP_ToolBox_{ver}"
)
current_dir = Path(__file__).parent

setup(
    name="TYNDP_ToolBox",
    version=ver.replace("v", "").replace("-", "."),
    description="TYNDP_ToolBox",
    options={
        "build_exe": build_exe_options,
        "install_exe": {"install_dir": str(dist_dir)},
    },
    executables=[
        Executable(
            "TYNDP_ToolBox.py",
            targetName="TYNDP_ToolBox.exe",
            base=None,
            icon="winPython.ico",
        )
    ],
)

# Génération des fichiers .bat permettant de lancer l'application
exe_file = fr'"%~dp0\TYNDP_ToolBox_{ver}\TYNDP_ToolBox.exe" normal'
exe_bat_file = open(dist_dir / ".." / "TYNDP_ToolBox.bat", "w")
exe_bat_file.write(exe_file)
exe_bat_file.close()

exe_file = fr'"%~dp0\TYNDP_ToolBox_{ver}\TYNDP_ToolBox.exe" batch'
exe_bat_file = open(dist_dir / ".." / "TYNDP_ToolBox_batch.bat", "w")
exe_bat_file.write(exe_file)
exe_bat_file.close()
help_file = fr'start "" "%~dp0\TYNDP_ToolBox_{ver}\help\html\index.html"'
help_bat_file = open(dist_dir / ".." / "help.bat", "w")
help_bat_file.write(help_file)
help_bat_file.close()

# Copie des dll liées à mkl
path_dll = Path(sys.exec_prefix) / "Library" / "bin"
for fichier in path_dll.glob("mkl*.dll"):
    shutil.copyfile(fichier, dist_dir / fichier.name)

# Rename Pool.pyc into pool.pyc
(dist_dir / "lib" / "multiprocessing" / "Pool.pyc").rename(dist_dir / "lib" / "multiprocessing" / "pool.pyc")

# Delete gen_py (Excel COM) in the distribution
try:
    shutil.rmtree(str(dist_dir / "lib" / "win32com" / "gen_py"))
except FileNotFoundError:
    pass

# Move data folder to root dir
try:
    shutil.move(str(dist_dir / "data"), str(dist_dir.parent))
except Exception:
    pass
