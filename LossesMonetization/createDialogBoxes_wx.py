# vim: set fileencoding=utf-8
import errno
import sys

import getpass
import os
from collections import deque
import tempfile
import traceback

import pandas as pd
import wx
import wx.lib.platebtn
import wx.html
from configparser import ConfigParser

import utils
import ExceptionPTDF
try:
    import _version
    __version__ = _version.__version__
except ImportError:
    __version__ = 'asv'

def set_text_entry(entry, value):
    if value:
        try:
            entry.Clear()
            entry.WriteText(value)
            entry.SetInsertionPoint(0)
            try:
                entry.SetToolTip(wx.ToolTip(os.path.basename(value)))
            except:
                pass
            wx.CallAfter(entry.SetInsertionPoint, 0)
        except:
            pass

class AboutBox(wx.Frame):

    def __init__(self, parent, title, text, parameters=None):
        import wx.grid as gridlib
        wx.Frame.__init__(self, parent, -1, title)
        panel = wx.Panel(self)
        self.parameters = parameters

        # Création Grid d'une seule ligne
        self.grid = gridlib.Grid(panel, size=(wx.DefaultSize[0], 100))
        self.grid.CreateGrid(1, len(parameters))

        # Création des bouton Copy-->Clipboard et Close
        button = wx.Button(panel, wx.ID_OK, "Copy \u279C Clipboard")
        button.Bind(wx.EVT_BUTTON, lambda evt: self.copy_clipboard(evt))
        sizer2 = wx.BoxSizer(wx.HORIZONTAL)
        button2 = wx.Button(panel, wx.ID_OK, "Close")
        sizer2.Add(button, 0, wx.ALIGN_CENTER | wx.ALL, 5)
        sizer2.Add(button2, 0, wx.ALIGN_CENTER | wx.ALL, 5)
        self.Bind(wx.EVT_BUTTON, self.on_close)

        # Police par défaut pour les cellules
        self.grid.SetDefaultCellFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD))
        # Police spéciale pour les paramètres optionnels
        font = wx.Font(10, wx.SWISS, wx.ITALIC, wx.NORMAL)

        # Remplissage de la Grid par les noms des paramètres attendus du fichier batch
        for iparam, param in enumerate(parameters):
            self.grid.SetCellValue(0, iparam, param['varConfig'])
            if 'hidden' in param or ('optionnel' in param and param['optionnel']):
                self.grid.SetCellFont(0, iparam, font)

        # Les colonnes s'ajustent automatiquement au contenu des cellules
        self.grid.AutoSize()

        # On supprime les bordures des cellules sélectionnées
        self.grid.SetCellHighlightPenWidth(0)

        # On empêche l'édition des celllules
        self.grid.EnableEditing(False)

        # Définition du callback appelé si clic sur cellule pour afficher la
        # copie d'écran du fichier attendu
        self.grid.Bind(gridlib.EVT_GRID_CELL_LEFT_CLICK, self.onRowClick)

        # put a tooltip on the cells in a column
        self.grid.GetGridWindow().Bind(wx.EVT_MOTION, self.onMouseOver)

        sizer = wx.BoxSizer(wx.VERTICAL)
        html_window = wx.html.HtmlWindow(panel, size=(wx.DefaultSize[0], 100))
        html_window.SetPage(text)
        sizer.Add(html_window, 0, wx.EXPAND)
        sizer.Add(self.grid, 0, wx.ALIGN_CENTER | wx.ALL, 5)
        sizer.Add(sizer2, 0, wx.ALIGN_CENTER | wx.ALL, 5)
        panel.SetSizer(sizer)
        sizer.Fit(self)
        self.SetMinSize(self.GetSize())
        self.SetMaxSize(self.GetSize())
        self.Centre()

    def onMouseOver(self, event):
        x, y = self.grid.CalcUnscrolledPosition(event.GetX(), event.GetY())
        coords = self.grid.XYToCell(x, y)
        col = coords[1]
        if col >= 0:
            # Note: This only sets the tooltip for the cells in the column
            try:
                # wxpython 4.0.0
                event.GetEventObject().SetToolTip(self.parameters[col].get('tooltip', ''))
            except:
                # wx 3.0.0
                event.GetEventObject().SetToolTipString(self.parameters[col].get('tooltip', ''))
        else:
            try:
                # wxpython 4.0.0
                event.GetEventObject().SetToolTip("")
            except:
                # wx 3.0.0
                event.GetEventObject().SetToolTipString("")

    def onRowClick(self, event):
        col = event.GetCol()
        # Récupération du répertoire courant (avec prise en compte du cas :
        # lancement de l'exécutable)
        if hasattr(sys, "frozen"):
            current_dir = os.path.dirname(sys.executable)
        else:
            current_dir = os.path.dirname(os.path.realpath(__file__))
        try:
            help_file = self.parameters[col]['help']
            if help_file.endswith('.png'):
                # load an image
                frame = wx.Frame(None, title='Help on file', size=(900, 200))
                panel_help = wx.Panel(frame)
                img = wx.Image(os.path.join(current_dir, help_file))
                image = wx.StaticBitmap(panel_help, wx.ID_ANY, wx.Bitmap(img))

                mainSizer = wx.BoxSizer(wx.VERTICAL)
                mainSizer.Add(image)

                # more generic setupcode
                panel_help.SetSizer(mainSizer)
                mainSizer.Fit(frame)
                panel_help.Layout()
                frame.Show(True)
            else:
                utils.display_html_file('Help',
                                        os.path.join(self.current_dir, help_file))
        except:
            pass

    def on_close(self, Event):
        self.Destroy()

    def copy_clipboard(self, evt):
        clipdata = wx.TextDataObject()
        clipdata.SetText("\t".join([x['varConfig'] for x in self.parameters]))
        wx.TheClipboard.Open()
        wx.TheClipboard.SetData(clipdata)
        wx.TheClipboard.Close()


class FileDrop(wx.FileDropTarget):
    """Classe permettant de gérer le drag and drop (glisser / déposer)."""
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window

    def OnDropFiles(self, x, y, filenames):
        for name in filenames:
            self.window.Clear()
            self.window.WriteText(name)
            try:
                self.window.SetToolTip(wx.ToolTip(os.path.basename(name)))
            except:
                pass
        return True



def make_dialog(cls, parent, title, module_name, parameters, start_function,
                     help_file=None, with_status_bar=False, log_std=None,
                     log_err=None, about=None, show_hidden_fields=False,
                     mode='normal', close_graphes=False):
    class createDialogBoxes(cls):
        def __init__(self, parent, title, module_name, parameters, start_function,
                     help_file=None, with_status_bar=False, log_std=None,
                     log_err=None, about=None, show_hidden_fields=False,
                     mode='normal', close_graphes=False):
            """Création d'une IHM permettant de sélectionner des fichiers, liste à
            choix, case à cocher."""
            super(createDialogBoxes, self).__init__(parent)
            try:
                self.SetScrollbars(0, 1, 0, 1)
            except AttributeError:
                pass
            self.frame = self.GetTopLevelParent()
            self.mode = mode
            self.start_function = start_function
            self.help_file = help_file
            self.close_graphes = close_graphes
            self.parent = parent
            self.log_std = log_std
            self.log_err = log_err
            self.last_dir = None
            self.show_hidden_fields = show_hidden_fields
            self.config = ConfigParser(interpolation=None)
            self.module_name = module_name.replace('.pyc', '').replace('.py', '')
    
            # Récupération du répertoire courant (avec prise en compte du cas :
            # lancement de l'exécutable)
            if hasattr(sys, "frozen"):
                self.current_dir = os.path.dirname(sys.executable)
            else:
                self.current_dir = os.path.dirname(os.path.realpath(__file__))
    
            # Association de l'icône à la fenêtre de l'IHM
            self.frame.SetTitle(title + ' [' + __version__.split('-')[0] + ']')
            try:
                icon = wx.Icon(os.path.join(self.current_dir, 'winPython.ico'), wx.BITMAP_TYPE_ICO)
                self.frame.SetIcon(icon)
            except:
                pass
    
            # Callback à appeler si clic sur la croix de la fenêtre (permettra d'éviter la fermeture de
            # la fenêtre quand un calcul est en cours)
            self.frame.Bind(wx.EVT_CLOSE, lambda evt, enable_close=True: self.on_close(evt, enable_close))
    
            # On va chercher le fichier de config dans le dossier conf
            config_filename = 'conf' + self.module_name + '_' + getpass.getuser() + '.cfg'
            try:
                with open(os.path.join(self.current_dir, 'conf', config_filename), 'r', encoding='utf8') as fic_config:
                    self.config.read_file(fic_config)
            except:
                # Si le dossier conf n'existe pas, on va lire le fichier de conf dans le répertoire courant
                try:
                    with open(os.path.join(self.current_dir, config_filename), 'r', encoding='utf8') as fic_config:
                        self.config.read_file(fic_config)
                except:
                    pass
    
            # Création de la barre de status
            if with_status_bar and isinstance(self.frame, wx.Frame):
                self.status_bar = self.frame.CreateStatusBar()
            else:
                self.status_bar = None
    
            self.entry = []
            self.popupmenu = []
            self.previous_entry = []
            self.params = parameters
    
            # Construction de l'IHM
            fgs = wx.GridBagSizer(hgap=5, vgap=5)
    
            no_line = 0
            for param in self.params:
                if 'separator' in param:
                    sep = wx.StaticText(self, -1, param['separator'])
                    sep.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD))
                    fgs.Add(sep, pos=(no_line, 0), flag=wx.LEFT | wx.TOP | wx.ALIGN_BOTTOM, border=10)
                    no_line += 1
                    fgs.Add(wx.StaticLine(self, -1), pos=(no_line, 0), span=(1, 3),
                            flag=wx.EXPAND | wx.LEFT | wx.RIGHT, border=10)
                    no_line += 1
    
                if 'hidden' not in param or self.show_hidden_fields:
    
                    # Création des champs texte de l'IHM (en fait un bouton clickable)
                    # pour pouvoir afficher l'aide si clic sur ce champ
                    label = myButton(self, param['text'])
                    label.SetWindowStyleFlag(wx.NO_BORDER)
                    try:
                        label.SetToolTip(wx.ToolTip(param['tooltip']))
                    except:
                        pass
                    if 'help' in param:
                        label.Bind(wx.EVT_BUTTON, lambda event, x=param['help'], y=param: self.show_file_template_help(event, x, y))
                    fgs.Add(label, pos=(no_line, 0), flag=wx.ALIGN_CENTER_VERTICAL)
    
                    # Initialisation de la liste des 10 derniers choix
                    previous_entry = []
    
                    # Pour les paramètres autres que liste à choix ou case à cocher
                    if param['action'][0] not in ("combo", "checkBox", "passwd"):
    
                        # Création du champ textuel de saisie
                        entry = wx.TextCtrl(self, -1, size=(500, -1), style=wx.TE_RICH)
    
                        # Mise à jour de la liste contenant les 10 derniers choix lus
                        # dans le fichier de configuration
                        previous_entry = deque([], 12)
    
                        # S'il existe un fichier batch généré lors d'une exécution à
                        # partir de l'IHM en mode 'normal'
                        if 'batch' in self.module_name:
                            gen_batch = os.path.join(self.current_dir,
                                                     'sav' + self.module_name + '.csv')
                            msg = utils.check_file_for_opening(gen_batch)
                            if msg[0] == 'Info':
                                previous_entry.append(gen_batch)
    
                        for i in range(10):
                            try:
                                prev = self.config.get("Filenames", str(param['varConfig'] + '[' + str(i) + ']'))
                                if prev and prev not in previous_entry:
                                    previous_entry.append(prev)
                            except:
                                pass
    
                        # Un clic-droit sur le champ textuel affichera la liste des 10 derniers choix
                        entry.Bind(wx.EVT_RIGHT_DOWN, lambda event, x=entry: self.OnShowPopup(event, x))
                        entry.Bind(wx.EVT_CONTEXT_MENU, self.skip)
                        entry.Bind(wx.EVT_RIGHT_DCLICK, self.skip)
    
                        fgs.Add(entry, pos=(no_line, 1), flag=wx.EXPAND | wx.ALIGN_CENTER_VERTICAL)
    
                        # Paramétrage du drag and drop (glisser / déposer)
                        dt = FileDrop(entry)
                        entry.SetDropTarget(dt)
                        entry.Bind(wx.EVT_MOTION, self.on_start_drag)
    
                        # Mise à jour du champ textuel à partir de la dernière valeur
                        # choisie (qui avait été sauvegardée dans le fichier de configuration)
                        default_value = param.get("default_value")
                        set_text_entry(entry, default_value)
                        try:
                            cur_value = self.config.get("Filenames", param['varConfig'])
                            set_text_entry(entry, cur_value)
                            if cur_value and cur_value not in previous_entry:
                                previous_entry.append(cur_value)
                            wx.CallAfter(entry.SetInsertionPoint, 0)
                        except:
                            pass
                        self.entry.append(entry)
    
                    # Cas des paramètres de type liste à choix
                    elif param['action'][0] == "combo":
    
                        # Création de la liste à choix à partir de la liste définie dans le paramètre
                        combo = wx.ComboBox(self, choices=[str(x) for x in param['action'][1]])
    
                        # Mise à jour de la liste à choix à partir de la dernière valeur
                        # choisie (qui avait été sauvegardée dans le fichier de configuration)
                        try:
                            combo.SetValue(str(self.config.get("Filenames", param['varConfig'])))
                        except:
                            try:
                                combo.SetValue(str(param['action'][1][0]))
                            except:
                                pass
                        fgs.Add(combo, pos=(no_line, 1), flag=wx.EXPAND | wx.ALIGN_CENTER_VERTICAL)
                        self.entry.append(combo)
    
                    # Cas des paramètres de type liste à choix
                    elif param['action'][0] == "checkBox":
    
                        # Création de la case à cocher
                        cb = wx.CheckBox(self)
    
                        # On lie la case à cocher à un callback qui permettra de rendre actif / inactif
                        # les champs liés
                        self.Bind(wx.EVT_CHECKBOX, lambda evt, x=param: self.set_entry_state(evt, x), cb)
    
                        # Mise à jour de la dernière valeur de la case à cocher
                        # (qui avait été sauvegardée dans le fichier de configuration)
                        try:
                            cb.SetValue(self.config.get("Filenames", param['varConfig']) == 'True')
                        except:
                            pass
    
                        fgs.Add(cb, pos=(no_line, 1), flag=wx.ALIGN_CENTER_VERTICAL)
                        self.entry.append(cb)
                    self.previous_entry.append(previous_entry)
    
                    if param['action'][0] == "passwd":
                        entry = wx.TextCtrl(self, -1, size=(500, -1), style=wx.TE_RICH | wx.TE_PASSWORD)
                        fgs.Add(entry, pos=(no_line, 1), flag=wx.EXPAND | wx.ALIGN_CENTER_VERTICAL)
                        self.entry.append(entry)
    
                    # Pour les paramètres de type "fichier" (open, save, ...)
                    if param['action'][0] not in ("combo", "checkBox", "entry", "passwd"):
                        try:
                            extra_args = param['action'][1]
                        except:
                            extra_args = {}
    
                        # Création d'un bouton contenant '...'
                        button = wx.Button(self, -1, label='...')
    
                        # Qui permettra de sélectionner le fichier / dossier
                        button.Bind(wx.EVT_BUTTON, lambda evt, x=param, y=param['action'][0], z=extra_args: self.choix_fichier(evt, x, y, **z))
    
                        fgs.Add(button,
                                pos=(no_line, 2),
                                flag=wx.ALIGN_CENTER_VERTICAL | wx.EXPAND | wx.RIGHT,
                                border=5)
                no_line += 1
    
            # Mise à jour de l'état des champs textuels éventuellement associés à une case à cocher
            for param in self.params:
                if param['action'][0] == "checkBox":
                    self.set_entry_state(None, param)
    
            if self.close_graphes or self.help_file:
                menubar = wx.MenuBar()
                if self.close_graphes:
                    close_menu = wx.Menu()
                    fitem = close_menu.Append(-1, 'Fermer tous les graphes',
                                              'Fermer tous les graphes')
                    self.frame.Bind(wx.EVT_MENU, self.on_close_graphes, fitem)
                    menubar.Append(close_menu, '&Graphes')
    
                # Création du menu Help (About / Principles)
                if self.help_file:
                    file_menu = wx.Menu()
                    fitem = file_menu.Append(-1, 'Principles', 'Principles')
                    self.frame.Bind(wx.EVT_MENU, self.onDisplayHelp, fitem)
                    if about:
                        self.about = about
                        file_menu.AppendSeparator()
                        about_item = file_menu.Append(-1, 'About', 'About')
                        self.frame.Bind(wx.EVT_MENU, self.on_about_click, about_item)
                    menubar.Append(file_menu, '&Help')
                self.frame.SetMenuBar(menubar)
    
            # Création des boutons OK et Fermer
            hbox = wx.BoxSizer(wx.HORIZONTAL)
            hbox.Add((20, 0), 1)
            if self.mode == 'batch':
                self.pbCheck = wx.Button(self, label='Check Batch', id=wx.ID_ADD)
                self.pbCheck.Bind(wx.EVT_BUTTON, lambda event: self.on_ok_click(event, True))
                hbox.Add(self.pbCheck, 0, wx.EXPAND, 10)
                hbox.Add((10, 0), 0.1)
                self.pbCheck.SetDefault()
    
            self.pbOk = wx.Button(self, label=_('Calculate'))
            if self.mode == 'normal':
                self.pbOk.SetDefault()
            self.pbOk.Bind(wx.EVT_BUTTON, lambda event: self.on_ok_click(event, False))
            hbox.Add(self.pbOk, 0, wx.EXPAND, 10)
            hbox.Add((10, 0), 0.1)
    
            self.pbCancel = wx.Button(self, label=_('Close'))
            self.pbCancel.Bind(wx.EVT_BUTTON, self.on_close)
            hbox.Add(self.pbCancel, 0, wx.EXPAND, 10)
            hbox.Add((20, 0), 1)
            fgs.Add(hbox, pos=(no_line, 1), flag=wx.EXPAND)
            no_line += 1
            fgs.AddGrowableCol(1, 1)
    
            self.SetSizer(fgs)
            fgs.Fit(self)
    
        def skip(self, Event):
            return
    
        def on_start_drag(self, evt):
            """"""
            if evt.Dragging():
                url = evt.GetEventObject().GetValue()
                data = wx.FileDataObject()
                data.AddFile(url)
    
                dropSource = wx.DropSource(evt.GetEventObject())
                dropSource.SetData(data)
                dropSource.DoDragDrop()
    
        def set_entry_state(self, Event, param):
            """Mise à jour de l'état des champs dépendant d'une case à cocher."""
            indice = self.params.index(param)
            for i in range(len(self.entry)):
                if 'depends_on' in self.params[i]:
                    if self.params[indice]['varConfig'] in self.params[i]['depends_on']:
                        if self.entry[indice].GetValue():
                            if 'not' in self.params[i]['depends_on']:
                                self.entry[i].Disable()
                            else:
                                self.entry[i].Enable()
                        else:
                            if 'not' in self.params[i]['depends_on']:
                                self.entry[i].Enable()
                            else:
                                self.entry[i].Disable()
    
        def on_about_click(self, event):
            """Affichage de la boîte de message About, contenant le numéro de version."""
            dial = wx.MessageDialog(None, self.about[1], self.about[0], wx.OK | wx.ICON_INFORMATION)
            dial.ShowModal()
    
        def on_close_graphes(self, event):
            """Permet de fermer tous les graphes ouverts lors du tracé des graphes."""
            import matplotlib.pyplot as plt
            plt.close("all")
    
        def onDisplayHelp(self, event):
            """Affichage de l'aide en ligne, suite à la sélection de Principles du
            menu Help."""
            utils.display_html_file('Help',
                                    os.path.join(self.current_dir, self.help_file))
    
        def OnShowPopup(self, event, entry):
            """Fonction appelée lors d'un clic droit sur champ textuel, affichant
            la liste des 10 dernières valeurs choisies."""
            popupmenu = wx.Menu()
            no_entry = self.entry.index(entry)
            for text in self.previous_entry[no_entry]:
                if entry.GetValue() != text:
                    item = popupmenu.Append(-1, text)
                    self.Bind(wx.EVT_MENU, lambda event, x=no_entry: self.OnPopupItemSelected(event, x), item)
    
            if len(self.previous_entry[no_entry]) > 0:
                x, y = event.GetEventObject().GetPosition()
                self.PopupMenu(popupmenu, [x, y + 10])
    
        def OnPopupItemSelected(self, event, no_entry):
            """Fonction appelée quand un élément de la liste des 10 dernières
            valeurs a été choisie : mise à jour du champ textuel."""
            text = event.GetEventObject().FindItemById(event.GetId()).GetText()
            self.entry[no_entry].SetValue(text)
    
        def show_file_template_help(self, event, image, param, text=''):
            """
               Fonction appelée pour afficher de l'aide contextuelle : format des
               fichiers en mode batch, affichage d'une image en mode normal.
            """
            # En mode batch : affichage du format attendu
            if 'help' in param and param['help'] == "get_parameters":
                html_help = text + "<br><br>" + _("The Header of this batch file must contain the following columns:")
    
                # On ajoute le paramètre repertoire_de_base, qui est présent et commun à tous les batchs
                parameters = [x for x in self.parameters if 'hidden' not in x or not x['hidden']]
                parametre_commun = {}
                parametre_commun['varConfig'] = 'repertoire_de_base'
                parametre_commun['tooltip'] = _("Base dir in which all other files will be looked for [optional column]")
                parametre_commun['optionnel'] = True
                parameters.append(parametre_commun)
                dlg = AboutBox(None, 'Help on format file', html_help, parameters)
                dlg.Show()
    
            # En mode normal : affichage d'une image si clic sur le label présentant le champ textuel
            else:
                try:
                    if image.endswith('.png'):
                        frame = wx.Frame(None, title='Help on file',
                                         size=(900, 200))
                        panel_help = wx.Panel(frame)
                        img = wx.Image(os.path.join(self.current_dir, image))
                        bitmap = wx.StaticBitmap(panel_help, wx.ID_ANY,
                                                 wx.Bitmap(img))
    
                        mainSizer = wx.BoxSizer(wx.VERTICAL)
                        mainSizer.Add(bitmap)
    
                        # more generic setupcode
                        panel_help.SetSizer(mainSizer)
                        mainSizer.Fit(frame)
                        panel_help.Layout()
                        frame.Show(True)
                    else:
                        utils.display_html_file('Help',
                                                os.path.join(self.current_dir, image))
                except:
                    pass
    
        def choix_fichier(self, Event, param, action, **kwargs):
            """ Choix du fichier à ouvrir ou sauvegarder """
            indice = self.params.index(param)
            fichier = ""
    
            # Mise à jour du dossier courant :
            # - dernier dossier s'il existe
            # - dossier défini par le champ de sélection de fichier s'il existe
            # - le répertoire Documents sinon
            if self.last_dir:
                last_dir = self.last_dir
            elif self.entry[indice].GetValue() != "":
                last_dir = os.path.dirname(self.entry[indice].GetValue())
            else:
                import win32com.client
                shell = win32com.client.Dispatch("WScript.Shell")
                last_dir = shell.SpecialFolders("MyDocuments")
    
            # Sélection d'un dossier
            if action == "openDir":
                dlg = wx.DirDialog(self, message="Choose Directory",
                                   defaultPath=last_dir, style=wx.RESIZE_BORDER)
                if dlg.ShowModal() == wx.ID_OK:
                    fichier = dlg.GetPath()
                    print(fichier)
                    dlg.Destroy()
    
            # Sélection d'un fichier pour ouverture en lecture
            if action == "open":
                if 'filetypes' in kwargs:
                    wildcards = kwargs['filetypes'][0][0] + '|' + kwargs['filetypes'][0][1]
                with wx.FileDialog(self,
                                   message="Open File",
                                   defaultDir=last_dir,
                                   defaultFile="",
                                   style=wx.FD_OPEN,
                                   wildcard=wildcards) as file_dialog:
                    if file_dialog.ShowModal() == wx.ID_CANCEL:
                        return
                    fichier = file_dialog.GetPath()
    
            # Sélection multiple de fichiers pour ouverture en lecture
            if action == "openMultiple":
                if 'filetypes' in kwargs:
                    wildcards = kwargs['filetypes'][0][0] + '|' + kwargs['filetypes'][0][1]
                dlg = wx.FileDialog(self, message="Open File", defaultDir=last_dir,
                                    defaultFile="", style=wx.FD_MULTIPLE | wx.FD_OPEN,
                                    wildcard=wildcards)
                if dlg.ShowModal() == wx.ID_OK:
                    fichier = dlg.GetPaths()
                    print(fichier)
                    dlg.Destroy()
    
            # Sélection d'un fichier en écriture
            elif action == "save":
                if 'filetypes' in kwargs:
                    wildcards = kwargs['filetypes'][0][0] + '|' + kwargs['filetypes'][0][1]
                dlg = wx.FileDialog(self, message="Save File", defaultDir=last_dir,
                                    defaultFile="", wildcard=wildcards,
                                    style=wx.FD_SAVE)
                if dlg.ShowModal() == wx.ID_OK:
                    fichier = dlg.GetPath()
                    print(fichier)
                    dlg.Destroy()
    
            if fichier != "":
    
                # Cas de la sélection multiple de fichiers : mise à jour du
                # champs textuel avec la liste des fichiers sélectionnés (séparés
                # par |)
                if isinstance(fichier, list):
                    self.entry[indice].SetValue('|'.join(fichier))
                    self.last_dir = os.path.dirname(fichier[0])
    
                # Mise à jour du champ textuel associé et du dossier par défaut
                else:
                    self.entry[indice].SetValue(fichier)
                    self.last_dir = os.path.dirname(fichier)
    
                # Mise à jour de la bulle d'aide
                try:
                    self.entry[indice].SetToolTip(wx.ToolTip(os.path.basename(fichier)))
                except:
                    pass
    
        def on_close(self, Event, enable_close=True):
            """Fonction appelée lors de la fermeture de la fenêtre en cliquant sur la croix."""
            if enable_close:
                self.frame.Destroy()
    
        def on_ok_click(self, Event, check_only=False):
            """Fonction appelée lors du clic sur le bouton OK pour lancer les calculs."""
            liste_params = []
            empty_filename = []
            for ligne, entry in enumerate(self.entry):
                if 'hidden' not in self.params[ligne] or self.show_hidden_fields:
                    liste_params.append(entry.GetValue())
    
                    # Fichier en lecture seule
                    if self.params[ligne]['action'][0] == "open":
    
                        # Lecture du nom du fichier saisi
                        filename = entry.GetValue()
    
                        # Nom de fichier vide mais optionnel : pas grave
                        if filename == "" and 'optional' in self.params[ligne]['action']:
                            pass
    
                        # Nom de fichier vide mais obligatoire : on affichera une erreur
                        elif filename == "":
                            empty_filename.append(self.params[ligne]['varConfig'])
    
                        # On essaie d'ouvrir le fichier en lecture
                        else:
                            try:
                                with open(filename, 'r'):
                                    pass
    
                                # Si la taille du fichier est nulle, on affiche une erreur
                                if os.path.getsize(filename) == 0:
                                    wx.MessageBox(
                                        _("Filename {} is empty").format(filename),
                                        _("Error while trying to open the file"),
                                        wx.OK | wx.ICON_ERROR
                                    )
                                    return
                            except IOError as exc:
                                wx.MessageBox(
                                    f"{filename} : {exc.strerror}",
                                    _("Error while trying to open the file"),
                                    wx.OK | wx.ICON_ERROR
                                )
                                return
    
                    # Fichier en écriture
                    if self.params[ligne]['action'][0] == "save":
                        # Lecture du nom du fichier saisi
                        filename = entry.GetValue()
    
                        # Nom de fichier vide mais optionnel : pas grave
                        if filename == "" and 'optional' in self.params[ligne]['action']:
                            pass
                        # Si le nom de fichier n'est pas vide, on essaie de l'ouvrir en écriture
                        elif filename != "":
                            try:
                                with open(filename, 'w'):
                                    pass
                            except PermissionError as exc:
                                wx.MessageBox(
                                    _("File [{}] is open in another application. Close it to continue.").format(filename),
                                    _("Impossible to write in the file"),
                                    wx.OK | wx.ICON_ERROR
                                )
                            except FileNotFoundError as exc:
                                wx.MessageBox(
                                    _("File [{}] contains an incorrect path or doesn't exist.").format(filename),
                                    _("Impossible to write in the file"),
                                    wx.OK | wx.ICON_ERROR
                                )
                            except IOError as exc:
                                wx.MessageBox(
                                    f"Error : {filename} : {exc.strerror}",
                                    _("Impossible to write in the file"),
                                    wx.OK | wx.ICON_ERROR
                                )
                                return
    
                        else:
                            empty_filename.append(self.params[ligne]['varConfig'])
            if empty_filename:
                wx.MessageBox(f'Empty filename(s) for {",".join(empty_filename)}',
                              "Empty filename",
                              wx.OK | wx.ICON_ERROR)
                return
    
            # Sauvegarde des champs saisis dans un fichier de configuration
            try:
                self.config.add_section("Filenames")
            except Exception:
                pass
    
            for ligne, entry in enumerate(self.entry):
                if 'hidden' not in self.params[ligne] or self.show_hidden_fields:
                    try:
                        value = entry.GetValue()
                        self.config.set("Filenames", str(self.params[ligne]['varConfig']), value)
                        if value not in self.previous_entry[ligne]:
                            self.previous_entry[ligne].append(value)
                    except:
                        value = entry.GetValue()
                        self.config.set("Filenames", str(self.params[ligne]['varConfig']), str(value))
    
                    try:
                        for i, previous in enumerate(self.previous_entry[ligne]):
                            try:
                                self.config.set("Filenames", str(self.params[ligne]['varConfig'] + '[' + str(i) + ']'), str(previous))
                            except:
                                self.config.set("Filenames", str(self.params[ligne]['varConfig']), str(previous))
                    except:
                        pass
            try:
                config_filename = 'conf' + self.module_name + '_' + getpass.getuser() + '.cfg'
                try:
                    rep_conf = os.path.join(self.current_dir, 'conf')
                    os.mkdir(rep_conf)
                except OSError as exc:  # Python >2.5
                    if not(exc.errno == errno.EEXIST and os.path.isdir(rep_conf)):
                        raise
                fic_config = open(os.path.join(rep_conf, config_filename), 'w',
                                  encoding='utf8')
            except Exception:
                fic_config = open(os.path.join(self.current_dir, config_filename),
                                  'w', encoding='utf8')
            self.config.write(fic_config)
            fic_config.close()
    
            parameters = dict((x, y)
                              for x, y in zip([param['varConfig'] for param in self.params],
                                              liste_params))
    
            # Sauvegarde des paramètres utilisés lors du lancement du module dans
            # un fichier qui sera utilisable en mode batch
            if 'batch' not in self.module_name:
                try:
                    (pd.DataFrame(parameters, index=['params'])
                     .to_csv(os.path.join(self.current_dir,
                                          'sav_batch_' + self.module_name + '.csv'),
                             index=False,
                             sep=';',
                             encoding='latin1'))
                except:
                    pass
    
            parameters['status_bar'] = self.status_bar
            if check_only:
                parameters['check_only'] = True
    
            try:
                self.pbOk.Disable()
                try:
                    self.pbCheck.Disable()
                except:
                    pass
                self.pbCancel.Disable()
                self.frame.Bind(wx.EVT_CLOSE, lambda evt, enable_close=False: self.on_close(evt, enable_close))
                wx.GetApp().Yield()
                self.start_function(**parameters)
                if 'batch' not in self.module_name:
                    self.frame.Bind(wx.EVT_CLOSE, lambda evt, enable_close=True: self.on_close(evt, enable_close))
            except ExceptionPTDF.ExceptionPTDF as exc:
                erreur = traceback.format_exc()
                print(erreur)
                print(exc)
                msg = exc.args[0]
                utils.log_or_display_msg(msg,
                                         self.log_std,
                                         self.log_err)
            except FileNotFoundError as exc:
                erreur = traceback.format_exc()
                print(erreur)
                print(exc)
                wx.MessageBox(
                    f"File Not Found\n\n"
                    f"Error during file read or file write\n"
                    f"[{exc}]",
                    "File Not Found",
                    wx.OK | wx.ICON_ERROR,
                    parent=self.parent
                )
            except PermissionError as exc:
                erreur = traceback.format_exc()
                print(erreur)
                print(exc)
                wx.MessageBox(
                    f"Permission Error\n\n"
                    f"Impossible to write into the file: file already opened ?\n"
                    f"[{exc}]",
                    "Permission Error",
                    wx.OK | wx.ICON_ERROR,
                    parent=self.parent
                )
            except OSError as exc:
                erreur = traceback.format_exc()
                print(erreur)
                print(exc)
                wx.MessageBox(
                    f"Perte de connexion au lecteur réseau ?\n\n"
                    f"Erreur lors de la lecture ou écriture d'un "
                    f"fichier : sans doute une perte de connexion au "
                    f"lecteur réseau.\n[{exc}]",
                    "Perte de connexion réseau",
                    wx.OK | wx.ICON_ERROR,
                    parent=self.parent
                )
            except:
                import gc
                gc.collect()
                self.pbOk.Enable()
                self.pbCancel.Enable()
                self.frame.Bind(wx.EVT_CLOSE, lambda evt, enable_close=True: self.on_close(evt, enable_close))
                erreur = traceback.format_exc()
                try:
                    self.pbCheck.Enable()
                except:
                    pass
    
                # Sauvegarde de l'erreur dans le presse-papier
                clipdata = wx.TextDataObject()
                try:
                    error_log = (
                        _("Unexpected error (TYNDPToolBox - Version {})\n\n{}\n\n{}").format(
                            __version__,
                            repr(parameters).replace(',', '\n'),
                            erreur
                        )
                    )
                    clipdata.SetText(error_log)
                except:
                    error_log = (
                        _("Unexpected error (TYNDPToolBox - Version {})\n\n{}").format(
                            __version__,
                            erreur
                        )
                    )
                    clipdata.SetText(error_log)
                wx.TheClipboard.Open()
                wx.TheClipboard.SetData(clipdata)
                wx.TheClipboard.Close()
    
                # Sauvegarde d'un fichier de log de l'erreur dans le dossier temp
                # D:\users\<user_name>\AppData\Local\bug_TYNDPToolBox.log
                try:
                    bug_report_filename = tempfile.mkstemp(
                        prefix='bug_TYNDPToolBox',
                        suffix='.log'
                    )
                    with open(bug_report_filename[1], 'w', encoding='utf8') as bug_file:
                        bug_file.writelines(error_log)
                except:
                    pass
    
                dlg = wx.GenericMessageDialog(
                    self.parent,
                    _("An error happened..."),
                    _("Unexpected error (TYNDPToolBox - Version {})").format(__version__),
                    wx.OK | wx.CANCEL | wx.ICON_ERROR
                )
                dlg.SetExtendedMessage(erreur)
                dlg.SetBackgroundColour(wx.WHITE)
                dlg.SetOKCancelLabels("Report bug in BitBucket", "Close")
                result = dlg.ShowModal()
                if result == wx.ID_OK:
                    utils.display_html_file(
                        "Report Bug on BitBucket",
                        "https://bitbucket.org/TYNDP_ToolBox/tyndp_toolbox/issues/new"
                    )
                dlg.Destroy()
                return
            if 'batch' not in self.module_name:
                self.pbOk.Enable()
                self.pbCancel.Enable()
                try:
                    self.pbCheck.Enable()
                except:
                    pass
            if check_only:
                self.pbOk.SetDefault()
            else:
                self.pbCancel.SetDefault()
    return createDialogBoxes(parent, title, module_name, parameters, start_function,
                     help_file, with_status_bar, log_std,
                     log_err, about, show_hidden_fields,
                     mode, close_graphes)

if __name__ == '__main__':
    parametresDialogBoxes = [
        {'text': 'Choose dir....', 'action': ['openDir'], 'varConfig': 'Dir'},
        {'text': 'Choose file to open .....', 'action': ['open', {'filetypes': [("csv files", "*.csv;*.zip")]}], 'varConfig': 'ficPTDFName'},
        {'text': 'entry0 .....', 'action': ['entry'], 'varConfig': 'entry0'},
        {'text': 'Choose file to save....', 'action': ['save', {'filetypes': [("csv files", "*.csv;*.zip")]}], 'varConfig': 'ficPTDFName0'},
        {'text': 'Choose another file to save....', 'action': ['save', {'filetypes': [("csv files", "*.csv;*.zip")]}], 'varConfig': 'ficPTDFName1'},
        {'text': 'fichier2 .....', 'action': ['entry'], 'varConfig': 'entry'},
        {'text': 'checkbutton', 'action': ['checkBox'], 'varConfig': 'cb1'},
        {'text': 'checkbutton2', 'action': ['checkBox'], 'varConfig': 'cb2'},
        {'text': 'fichier2 .....', 'action': ['combo', [1, 2, 3]], 'varConfig': 'choice2'},
        {'text': 'fichier2 .....', 'action': ['combo', ['un', 'deux', 'trois']], 'varConfig': 'choice'}
    ]


class myButton(wx.Button):
    def __init__(self, panel, label):
        wx.Button.__init__(self,
                           panel,
                           label=label)
        self.SetLabelMarkup(label)

    def AcceptsFocus(self):
            return False
