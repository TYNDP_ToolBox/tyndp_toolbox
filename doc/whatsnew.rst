.. _whatsnew:

**********
What's New
**********

.. include:: whatsnew/v0.3.txt
   :encoding: utf-8

.. include:: whatsnew/v0.2.txt
   :encoding: utf-8
