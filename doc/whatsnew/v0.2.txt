..  vim: set fileencoding=utf-8
.. _whatsnew_0002:

v0.2 (2 April 2020)
===================

.. contents:: Nouveautés v0.2
    :local:
    :backlinks: none

.. _whatsnew_0002_contents2:

First official release of the TYNDP ToolBox. This toolbox contains a few
modules:

- to extract Marginal Costs from Market Output files,
- to calculates **Losses Monetization**,
- to calculate **CO2 emission** and **CO2 monetization** due to losses
