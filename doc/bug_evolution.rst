.. _bugs_evolutions_intro:

Reporting a bug
===============

It is possible to report a bug or to ask for an evolution in BitBucket:

https://bitbucket.org/TYNDP_ToolBox/tyndp_toolbox/issues/new

.. note::


   In case an unexpected error would occur, an error message will show up.

   Clicking on the button :guilabel:`Report Bug in BitBucket` will allow:

   - to access directly to the `New Issue` creation in BitBucket,
   - to copy in the ClipBoard the content of the error message which will be
     very useful to analyze the bug: using Paste (CTRL-V) in the
     **Description** field of the New Issue will copy the error message.
