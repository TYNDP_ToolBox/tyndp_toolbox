************
Installation
************

From source
===========

- Download the source of Losses Monetization Tool that can be found here:

  https://bitbucket.org/TYNDP_ToolBox/tyndp_toolbox/src/master

  A zip file can be obtained from BitBucket:

.. image:: images/download_code_bitbucket.png
  
- unzip the downloaded zip

- Download MiniConda (see <https://docs.conda.io/en/latest/miniconda.html>)

- Install all the needed packages in the LossesMonetization virtual environment:

  .. code-block:: none

    conda env create -n LossesMonetization -f environment.yml

- Activate the LossesMonetization environment:

  .. code-block:: none

    activate LossesMonetization

- Run the ToolBox in GUI mode:

  .. code-block:: none

    python losses_monetization_gui.py

- Run the ToolBox in batch mode:

  .. code-block:: none

    python losses_monetization_batch.py

From exe
========

- Download the zip from the Sharefile: <https://entsoe.sharefile.com/home/shared/fo58a6f0-29b6-4419-9701-133283726876>
- Unzip the archive in any folder, 
- Go to the ``TYNDP_ToolBox_vx.y-z`` folder
- Run the ToolBox in GUI mode by executing ``TYNDP_ToolBox.bat``
- Run the ToolBox in batch mode by executing ``TYNDP_ToolBox_batch.bat``
