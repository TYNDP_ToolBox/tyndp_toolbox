.. python4PTDF documentation master file, created by
   sphinx-quickstart on Mon Jan 05 21:08:23 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to TYNDP ToolBox documentation!
=======================================

Table des matières :

.. toctree::
   :maxdepth: 2
   :caption: Installation

   install

.. toctree::
   :maxdepth: 2
   :caption: Modules

   extract_market_output
   losses_monetization
   co2_emission

.. toctree::
   :maxdepth: 2
   :caption: Misc

   mode_batch
   bug_evolution
   whatsnew
